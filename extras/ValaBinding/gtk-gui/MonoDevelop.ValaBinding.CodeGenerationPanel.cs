// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace MonoDevelop.ValaBinding {
    
    
    public partial class CodeGenerationPanel {
        
        private Gtk.Notebook notebook1;
        
        private Gtk.VBox vbox6;
        
        private Gtk.Table table1;
        
        private Gtk.Label label10;
        
        private Gtk.Label label4;
        
        private Gtk.Label label5;
        
        private Gtk.Label label6;
        
        private Gtk.SpinButton optimizationSpinButton;
        
        private Gtk.ComboBox targetComboBox;
        
        private Gtk.CheckButton threadingCheckbox;
        
        private Gtk.VBox vbox1;
        
        private Gtk.RadioButton noWarningRadio;
        
        private Gtk.RadioButton normalWarningRadio;
        
        private Gtk.RadioButton allWarningRadio;
        
        private Gtk.CheckButton warningsAsErrorsCheckBox;
        
        private Gtk.HBox hbox1;
        
        private Gtk.Label label12;
        
        private Gtk.Entry defineSymbolsTextEntry;
        
        private Gtk.Frame frame2;
        
        private Gtk.Alignment GtkAlignment;
        
        private Gtk.Table table5;
        
        private Gtk.Label label7;
        
        private Gtk.ScrolledWindow scrolledwindow4;
        
        private Gtk.TextView extraCompilerTextView;
        
        private Gtk.Label GtkLabel12;
        
        private Gtk.Label label1;
        
        private Gtk.Table table2;
        
        private Gtk.Button addLibButton;
        
        private Gtk.Label label8;
        
        private Gtk.Entry libAddEntry;
        
        private Gtk.ScrolledWindow scrolledwindow1;
        
        private Gtk.TreeView libTreeView;
        
        private Gtk.VBox vbox4;
        
        private Gtk.Button browseButton;
        
        private Gtk.Button removeLibButton;
        
        private Gtk.Label label2;
        
        private Gtk.VBox vbox7;
        
        private Gtk.Table table3;
        
        private Gtk.Button includePathAddButton;
        
        private Gtk.Entry includePathEntry;
        
        private Gtk.Label label9;
        
        private Gtk.ScrolledWindow scrolledwindow2;
        
        private Gtk.TreeView includePathTreeView;
        
        private Gtk.VBox vbox5;
        
        private Gtk.Button includePathBrowseButton;
        
        private Gtk.Button includePathRemoveButton;
        
        private Gtk.Label label3;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget MonoDevelop.ValaBinding.CodeGenerationPanel
            Stetic.BinContainer.Attach(this);
            this.Name = "MonoDevelop.ValaBinding.CodeGenerationPanel";
            // Container child MonoDevelop.ValaBinding.CodeGenerationPanel.Gtk.Container+ContainerChild
            this.notebook1 = new Gtk.Notebook();
            this.notebook1.CanFocus = true;
            this.notebook1.Name = "notebook1";
            this.notebook1.CurrentPage = 0;
            // Container child notebook1.Gtk.Notebook+NotebookChild
            this.vbox6 = new Gtk.VBox();
            this.vbox6.Name = "vbox6";
            this.vbox6.Spacing = 3;
            // Container child vbox6.Gtk.Box+BoxChild
            this.table1 = new Gtk.Table(((uint)(4)), ((uint)(2)), false);
            this.table1.Name = "table1";
            this.table1.RowSpacing = ((uint)(5));
            this.table1.ColumnSpacing = ((uint)(5));
            this.table1.BorderWidth = ((uint)(2));
            // Container child table1.Gtk.Table+TableChild
            this.label10 = new Gtk.Label();
            this.label10.Name = "label10";
            this.label10.Xpad = 10;
            this.label10.Xalign = 0F;
            this.label10.LabelProp = Mono.Unix.Catalog.GetString("Warning Level:");
            this.table1.Add(this.label10);
            Gtk.Table.TableChild w1 = ((Gtk.Table.TableChild)(this.table1[this.label10]));
            w1.XOptions = ((Gtk.AttachOptions)(4));
            w1.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label4 = new Gtk.Label();
            this.label4.Name = "label4";
            this.label4.Xpad = 10;
            this.label4.Xalign = 0F;
            this.label4.LabelProp = Mono.Unix.Catalog.GetString("Multithreading:");
            this.table1.Add(this.label4);
            Gtk.Table.TableChild w2 = ((Gtk.Table.TableChild)(this.table1[this.label4]));
            w2.TopAttach = ((uint)(3));
            w2.BottomAttach = ((uint)(4));
            w2.XOptions = ((Gtk.AttachOptions)(4));
            w2.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label5 = new Gtk.Label();
            this.label5.Name = "label5";
            this.label5.Xpad = 10;
            this.label5.Xalign = 0F;
            this.label5.LabelProp = Mono.Unix.Catalog.GetString("Optimization Level:");
            this.table1.Add(this.label5);
            Gtk.Table.TableChild w3 = ((Gtk.Table.TableChild)(this.table1[this.label5]));
            w3.TopAttach = ((uint)(1));
            w3.BottomAttach = ((uint)(2));
            w3.XOptions = ((Gtk.AttachOptions)(4));
            w3.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label6 = new Gtk.Label();
            this.label6.Name = "label6";
            this.label6.Xpad = 10;
            this.label6.Xalign = 0F;
            this.label6.LabelProp = Mono.Unix.Catalog.GetString("Target:");
            this.table1.Add(this.label6);
            Gtk.Table.TableChild w4 = ((Gtk.Table.TableChild)(this.table1[this.label6]));
            w4.TopAttach = ((uint)(2));
            w4.BottomAttach = ((uint)(3));
            w4.XOptions = ((Gtk.AttachOptions)(4));
            w4.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.optimizationSpinButton = new Gtk.SpinButton(0, 3, 1);
            this.optimizationSpinButton.Sensitive = false;
            this.optimizationSpinButton.CanFocus = true;
            this.optimizationSpinButton.Name = "optimizationSpinButton";
            this.optimizationSpinButton.Adjustment.PageIncrement = 10;
            this.optimizationSpinButton.ClimbRate = 1;
            this.optimizationSpinButton.Numeric = true;
            this.table1.Add(this.optimizationSpinButton);
            Gtk.Table.TableChild w5 = ((Gtk.Table.TableChild)(this.table1[this.optimizationSpinButton]));
            w5.TopAttach = ((uint)(1));
            w5.BottomAttach = ((uint)(2));
            w5.LeftAttach = ((uint)(1));
            w5.RightAttach = ((uint)(2));
            w5.XOptions = ((Gtk.AttachOptions)(4));
            w5.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.targetComboBox = Gtk.ComboBox.NewText();
            this.targetComboBox.AppendText(Mono.Unix.Catalog.GetString("Executable"));
            this.targetComboBox.AppendText(Mono.Unix.Catalog.GetString("Static Library"));
            this.targetComboBox.AppendText(Mono.Unix.Catalog.GetString("Shared Object"));
            this.targetComboBox.Name = "targetComboBox";
            this.table1.Add(this.targetComboBox);
            Gtk.Table.TableChild w6 = ((Gtk.Table.TableChild)(this.table1[this.targetComboBox]));
            w6.TopAttach = ((uint)(2));
            w6.BottomAttach = ((uint)(3));
            w6.LeftAttach = ((uint)(1));
            w6.RightAttach = ((uint)(2));
            w6.XOptions = ((Gtk.AttachOptions)(4));
            w6.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.threadingCheckbox = new Gtk.CheckButton();
            this.threadingCheckbox.CanFocus = true;
            this.threadingCheckbox.Name = "threadingCheckbox";
            this.threadingCheckbox.Label = Mono.Unix.Catalog.GetString("Enable multithreading");
            this.threadingCheckbox.DrawIndicator = true;
            this.threadingCheckbox.UseUnderline = true;
            this.table1.Add(this.threadingCheckbox);
            Gtk.Table.TableChild w7 = ((Gtk.Table.TableChild)(this.table1[this.threadingCheckbox]));
            w7.TopAttach = ((uint)(3));
            w7.BottomAttach = ((uint)(4));
            w7.LeftAttach = ((uint)(1));
            w7.RightAttach = ((uint)(2));
            w7.XOptions = ((Gtk.AttachOptions)(4));
            w7.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.vbox1 = new Gtk.VBox();
            this.vbox1.Name = "vbox1";
            this.vbox1.Spacing = 1;
            // Container child vbox1.Gtk.Box+BoxChild
            this.noWarningRadio = new Gtk.RadioButton(Mono.Unix.Catalog.GetString("no warnings"));
            this.noWarningRadio.CanFocus = true;
            this.noWarningRadio.Name = "noWarningRadio";
            this.noWarningRadio.Active = true;
            this.noWarningRadio.DrawIndicator = true;
            this.noWarningRadio.UseUnderline = true;
            this.noWarningRadio.Group = new GLib.SList(System.IntPtr.Zero);
            this.vbox1.Add(this.noWarningRadio);
            Gtk.Box.BoxChild w8 = ((Gtk.Box.BoxChild)(this.vbox1[this.noWarningRadio]));
            w8.Position = 0;
            w8.Expand = false;
            w8.Fill = false;
            // Container child vbox1.Gtk.Box+BoxChild
            this.normalWarningRadio = new Gtk.RadioButton(Mono.Unix.Catalog.GetString("normal"));
            this.normalWarningRadio.CanFocus = true;
            this.normalWarningRadio.Name = "normalWarningRadio";
            this.normalWarningRadio.DrawIndicator = true;
            this.normalWarningRadio.UseUnderline = true;
            this.normalWarningRadio.Group = this.noWarningRadio.Group;
            this.vbox1.Add(this.normalWarningRadio);
            Gtk.Box.BoxChild w9 = ((Gtk.Box.BoxChild)(this.vbox1[this.normalWarningRadio]));
            w9.Position = 1;
            w9.Expand = false;
            w9.Fill = false;
            // Container child vbox1.Gtk.Box+BoxChild
            this.allWarningRadio = new Gtk.RadioButton(Mono.Unix.Catalog.GetString("all"));
            this.allWarningRadio.CanFocus = true;
            this.allWarningRadio.Name = "allWarningRadio";
            this.allWarningRadio.DrawIndicator = true;
            this.allWarningRadio.UseUnderline = true;
            this.allWarningRadio.Group = this.noWarningRadio.Group;
            this.vbox1.Add(this.allWarningRadio);
            Gtk.Box.BoxChild w10 = ((Gtk.Box.BoxChild)(this.vbox1[this.allWarningRadio]));
            w10.Position = 2;
            w10.Expand = false;
            w10.Fill = false;
            // Container child vbox1.Gtk.Box+BoxChild
            this.warningsAsErrorsCheckBox = new Gtk.CheckButton();
            this.warningsAsErrorsCheckBox.Sensitive = false;
            this.warningsAsErrorsCheckBox.CanFocus = true;
            this.warningsAsErrorsCheckBox.Name = "warningsAsErrorsCheckBox";
            this.warningsAsErrorsCheckBox.Label = Mono.Unix.Catalog.GetString("Treat warnings as errors");
            this.warningsAsErrorsCheckBox.DrawIndicator = true;
            this.warningsAsErrorsCheckBox.UseUnderline = true;
            this.vbox1.Add(this.warningsAsErrorsCheckBox);
            Gtk.Box.BoxChild w11 = ((Gtk.Box.BoxChild)(this.vbox1[this.warningsAsErrorsCheckBox]));
            w11.Position = 3;
            w11.Expand = false;
            w11.Fill = false;
            this.table1.Add(this.vbox1);
            Gtk.Table.TableChild w12 = ((Gtk.Table.TableChild)(this.table1[this.vbox1]));
            w12.LeftAttach = ((uint)(1));
            w12.RightAttach = ((uint)(2));
            w12.XOptions = ((Gtk.AttachOptions)(4));
            w12.YOptions = ((Gtk.AttachOptions)(4));
            this.vbox6.Add(this.table1);
            Gtk.Box.BoxChild w13 = ((Gtk.Box.BoxChild)(this.vbox6[this.table1]));
            w13.Position = 0;
            w13.Expand = false;
            w13.Fill = false;
            // Container child vbox6.Gtk.Box+BoxChild
            this.hbox1 = new Gtk.HBox();
            this.hbox1.Name = "hbox1";
            this.hbox1.Spacing = 6;
            // Container child hbox1.Gtk.Box+BoxChild
            this.label12 = new Gtk.Label();
            this.label12.Name = "label12";
            this.label12.Xpad = 13;
            this.label12.Xalign = 0F;
            this.label12.LabelProp = Mono.Unix.Catalog.GetString("Define Symbols:");
            this.hbox1.Add(this.label12);
            Gtk.Box.BoxChild w14 = ((Gtk.Box.BoxChild)(this.hbox1[this.label12]));
            w14.Position = 0;
            w14.Expand = false;
            w14.Fill = false;
            // Container child hbox1.Gtk.Box+BoxChild
            this.defineSymbolsTextEntry = new Gtk.Entry();
            Gtk.Tooltips w15 = new Gtk.Tooltips();
            w15.SetTip(this.defineSymbolsTextEntry, "A space-separated list of symbols to define.", "A space-separated list of symbols to define.");
            this.defineSymbolsTextEntry.CanFocus = true;
            this.defineSymbolsTextEntry.Name = "defineSymbolsTextEntry";
            this.defineSymbolsTextEntry.IsEditable = true;
            this.defineSymbolsTextEntry.InvisibleChar = '●';
            this.hbox1.Add(this.defineSymbolsTextEntry);
            Gtk.Box.BoxChild w16 = ((Gtk.Box.BoxChild)(this.hbox1[this.defineSymbolsTextEntry]));
            w16.Position = 1;
            w16.Padding = ((uint)(14));
            this.vbox6.Add(this.hbox1);
            Gtk.Box.BoxChild w17 = ((Gtk.Box.BoxChild)(this.vbox6[this.hbox1]));
            w17.Position = 1;
            w17.Expand = false;
            w17.Fill = false;
            // Container child vbox6.Gtk.Box+BoxChild
            this.frame2 = new Gtk.Frame();
            this.frame2.Name = "frame2";
            this.frame2.ShadowType = ((Gtk.ShadowType)(0));
            this.frame2.LabelYalign = 0F;
            // Container child frame2.Gtk.Container+ContainerChild
            this.GtkAlignment = new Gtk.Alignment(0F, 0F, 1F, 1F);
            this.GtkAlignment.Name = "GtkAlignment";
            this.GtkAlignment.LeftPadding = ((uint)(12));
            // Container child GtkAlignment.Gtk.Container+ContainerChild
            this.table5 = new Gtk.Table(((uint)(2)), ((uint)(1)), false);
            this.table5.Name = "table5";
            this.table5.RowSpacing = ((uint)(6));
            this.table5.ColumnSpacing = ((uint)(9));
            this.table5.BorderWidth = ((uint)(6));
            // Container child table5.Gtk.Table+TableChild
            this.label7 = new Gtk.Label();
            this.label7.Name = "label7";
            this.label7.Xalign = 0F;
            this.label7.LabelProp = Mono.Unix.Catalog.GetString("Extra Compiler Options");
            this.table5.Add(this.label7);
            Gtk.Table.TableChild w18 = ((Gtk.Table.TableChild)(this.table5[this.label7]));
            w18.XOptions = ((Gtk.AttachOptions)(4));
            w18.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table5.Gtk.Table+TableChild
            this.scrolledwindow4 = new Gtk.ScrolledWindow();
            this.scrolledwindow4.CanFocus = true;
            this.scrolledwindow4.Name = "scrolledwindow4";
            this.scrolledwindow4.ShadowType = ((Gtk.ShadowType)(1));
            // Container child scrolledwindow4.Gtk.Container+ContainerChild
            this.extraCompilerTextView = new Gtk.TextView();
            w15.SetTip(this.extraCompilerTextView, "A newline-separated list of extra options to send to the compiler.\nOne option can be in more than one line.\nExample:\n\t`--pkg\n\tcairo`", "A newline-separated list of extra options to send to the compiler.\nOne option can be in more than one line.\nExample:\n\t`--pkg\n\tcairo`");
            this.extraCompilerTextView.CanFocus = true;
            this.extraCompilerTextView.Name = "extraCompilerTextView";
            this.scrolledwindow4.Add(this.extraCompilerTextView);
            this.table5.Add(this.scrolledwindow4);
            Gtk.Table.TableChild w20 = ((Gtk.Table.TableChild)(this.table5[this.scrolledwindow4]));
            w20.TopAttach = ((uint)(1));
            w20.BottomAttach = ((uint)(2));
            this.GtkAlignment.Add(this.table5);
            this.frame2.Add(this.GtkAlignment);
            this.GtkLabel12 = new Gtk.Label();
            this.GtkLabel12.Name = "GtkLabel12";
            this.GtkLabel12.LabelProp = Mono.Unix.Catalog.GetString("<b>Extra Options</b>");
            this.GtkLabel12.UseMarkup = true;
            this.frame2.LabelWidget = this.GtkLabel12;
            this.vbox6.Add(this.frame2);
            Gtk.Box.BoxChild w23 = ((Gtk.Box.BoxChild)(this.vbox6[this.frame2]));
            w23.Position = 2;
            this.notebook1.Add(this.vbox6);
            // Notebook tab
            this.label1 = new Gtk.Label();
            this.label1.Name = "label1";
            this.label1.LabelProp = Mono.Unix.Catalog.GetString("Code Generation");
            this.notebook1.SetTabLabel(this.vbox6, this.label1);
            this.label1.ShowAll();
            // Container child notebook1.Gtk.Notebook+NotebookChild
            this.table2 = new Gtk.Table(((uint)(2)), ((uint)(3)), false);
            this.table2.Name = "table2";
            this.table2.RowSpacing = ((uint)(10));
            this.table2.ColumnSpacing = ((uint)(10));
            this.table2.BorderWidth = ((uint)(3));
            // Container child table2.Gtk.Table+TableChild
            this.addLibButton = new Gtk.Button();
            this.addLibButton.Sensitive = false;
            this.addLibButton.CanFocus = true;
            this.addLibButton.Name = "addLibButton";
            this.addLibButton.UseUnderline = true;
            this.addLibButton.Label = Mono.Unix.Catalog.GetString("Add");
            this.table2.Add(this.addLibButton);
            Gtk.Table.TableChild w25 = ((Gtk.Table.TableChild)(this.table2[this.addLibButton]));
            w25.LeftAttach = ((uint)(2));
            w25.RightAttach = ((uint)(3));
            w25.XOptions = ((Gtk.AttachOptions)(4));
            w25.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table2.Gtk.Table+TableChild
            this.label8 = new Gtk.Label();
            this.label8.Name = "label8";
            this.label8.LabelProp = Mono.Unix.Catalog.GetString("Library:");
            this.table2.Add(this.label8);
            Gtk.Table.TableChild w26 = ((Gtk.Table.TableChild)(this.table2[this.label8]));
            w26.XOptions = ((Gtk.AttachOptions)(4));
            w26.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table2.Gtk.Table+TableChild
            this.libAddEntry = new Gtk.Entry();
            this.libAddEntry.CanFocus = true;
            this.libAddEntry.Name = "libAddEntry";
            this.libAddEntry.IsEditable = true;
            this.libAddEntry.InvisibleChar = '●';
            this.table2.Add(this.libAddEntry);
            Gtk.Table.TableChild w27 = ((Gtk.Table.TableChild)(this.table2[this.libAddEntry]));
            w27.LeftAttach = ((uint)(1));
            w27.RightAttach = ((uint)(2));
            w27.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table2.Gtk.Table+TableChild
            this.scrolledwindow1 = new Gtk.ScrolledWindow();
            this.scrolledwindow1.CanFocus = true;
            this.scrolledwindow1.Name = "scrolledwindow1";
            this.scrolledwindow1.ShadowType = ((Gtk.ShadowType)(1));
            // Container child scrolledwindow1.Gtk.Container+ContainerChild
            this.libTreeView = new Gtk.TreeView();
            this.libTreeView.CanFocus = true;
            this.libTreeView.Name = "libTreeView";
            this.scrolledwindow1.Add(this.libTreeView);
            this.table2.Add(this.scrolledwindow1);
            Gtk.Table.TableChild w29 = ((Gtk.Table.TableChild)(this.table2[this.scrolledwindow1]));
            w29.TopAttach = ((uint)(1));
            w29.BottomAttach = ((uint)(2));
            w29.LeftAttach = ((uint)(1));
            w29.RightAttach = ((uint)(2));
            // Container child table2.Gtk.Table+TableChild
            this.vbox4 = new Gtk.VBox();
            this.vbox4.Name = "vbox4";
            this.vbox4.Spacing = 6;
            // Container child vbox4.Gtk.Box+BoxChild
            this.browseButton = new Gtk.Button();
            this.browseButton.CanFocus = true;
            this.browseButton.Name = "browseButton";
            this.browseButton.UseUnderline = true;
            this.browseButton.Label = Mono.Unix.Catalog.GetString("Browse...");
            this.vbox4.Add(this.browseButton);
            Gtk.Box.BoxChild w30 = ((Gtk.Box.BoxChild)(this.vbox4[this.browseButton]));
            w30.Position = 0;
            w30.Expand = false;
            w30.Fill = false;
            // Container child vbox4.Gtk.Box+BoxChild
            this.removeLibButton = new Gtk.Button();
            this.removeLibButton.Sensitive = false;
            this.removeLibButton.CanFocus = true;
            this.removeLibButton.Name = "removeLibButton";
            this.removeLibButton.UseUnderline = true;
            this.removeLibButton.Label = Mono.Unix.Catalog.GetString("Remove");
            this.vbox4.Add(this.removeLibButton);
            Gtk.Box.BoxChild w31 = ((Gtk.Box.BoxChild)(this.vbox4[this.removeLibButton]));
            w31.Position = 1;
            w31.Expand = false;
            w31.Fill = false;
            this.table2.Add(this.vbox4);
            Gtk.Table.TableChild w32 = ((Gtk.Table.TableChild)(this.table2[this.vbox4]));
            w32.TopAttach = ((uint)(1));
            w32.BottomAttach = ((uint)(2));
            w32.LeftAttach = ((uint)(2));
            w32.RightAttach = ((uint)(3));
            w32.XOptions = ((Gtk.AttachOptions)(4));
            this.notebook1.Add(this.table2);
            Gtk.Notebook.NotebookChild w33 = ((Gtk.Notebook.NotebookChild)(this.notebook1[this.table2]));
            w33.Position = 1;
            // Notebook tab
            this.label2 = new Gtk.Label();
            this.label2.Name = "label2";
            this.label2.LabelProp = Mono.Unix.Catalog.GetString("Libraries");
            this.notebook1.SetTabLabel(this.table2, this.label2);
            this.label2.ShowAll();
            // Container child notebook1.Gtk.Notebook+NotebookChild
            this.vbox7 = new Gtk.VBox();
            this.vbox7.Name = "vbox7";
            this.vbox7.Spacing = 6;
            this.vbox7.BorderWidth = ((uint)(3));
            // Container child vbox7.Gtk.Box+BoxChild
            this.table3 = new Gtk.Table(((uint)(2)), ((uint)(3)), false);
            this.table3.Name = "table3";
            this.table3.RowSpacing = ((uint)(10));
            this.table3.ColumnSpacing = ((uint)(10));
            // Container child table3.Gtk.Table+TableChild
            this.includePathAddButton = new Gtk.Button();
            this.includePathAddButton.Sensitive = false;
            this.includePathAddButton.CanFocus = true;
            this.includePathAddButton.Name = "includePathAddButton";
            this.includePathAddButton.UseUnderline = true;
            this.includePathAddButton.Label = Mono.Unix.Catalog.GetString("Add");
            this.table3.Add(this.includePathAddButton);
            Gtk.Table.TableChild w34 = ((Gtk.Table.TableChild)(this.table3[this.includePathAddButton]));
            w34.LeftAttach = ((uint)(2));
            w34.RightAttach = ((uint)(3));
            w34.XOptions = ((Gtk.AttachOptions)(4));
            w34.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table3.Gtk.Table+TableChild
            this.includePathEntry = new Gtk.Entry();
            this.includePathEntry.CanFocus = true;
            this.includePathEntry.Name = "includePathEntry";
            this.includePathEntry.IsEditable = true;
            this.includePathEntry.InvisibleChar = '●';
            this.table3.Add(this.includePathEntry);
            Gtk.Table.TableChild w35 = ((Gtk.Table.TableChild)(this.table3[this.includePathEntry]));
            w35.LeftAttach = ((uint)(1));
            w35.RightAttach = ((uint)(2));
            w35.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table3.Gtk.Table+TableChild
            this.label9 = new Gtk.Label();
            this.label9.Name = "label9";
            this.label9.LabelProp = Mono.Unix.Catalog.GetString("Vapi Paths:");
            this.table3.Add(this.label9);
            Gtk.Table.TableChild w36 = ((Gtk.Table.TableChild)(this.table3[this.label9]));
            w36.XOptions = ((Gtk.AttachOptions)(4));
            w36.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table3.Gtk.Table+TableChild
            this.scrolledwindow2 = new Gtk.ScrolledWindow();
            this.scrolledwindow2.CanFocus = true;
            this.scrolledwindow2.Name = "scrolledwindow2";
            this.scrolledwindow2.ShadowType = ((Gtk.ShadowType)(1));
            // Container child scrolledwindow2.Gtk.Container+ContainerChild
            this.includePathTreeView = new Gtk.TreeView();
            this.includePathTreeView.CanFocus = true;
            this.includePathTreeView.Name = "includePathTreeView";
            this.scrolledwindow2.Add(this.includePathTreeView);
            this.table3.Add(this.scrolledwindow2);
            Gtk.Table.TableChild w38 = ((Gtk.Table.TableChild)(this.table3[this.scrolledwindow2]));
            w38.TopAttach = ((uint)(1));
            w38.BottomAttach = ((uint)(2));
            w38.LeftAttach = ((uint)(1));
            w38.RightAttach = ((uint)(2));
            // Container child table3.Gtk.Table+TableChild
            this.vbox5 = new Gtk.VBox();
            this.vbox5.Name = "vbox5";
            this.vbox5.Spacing = 6;
            // Container child vbox5.Gtk.Box+BoxChild
            this.includePathBrowseButton = new Gtk.Button();
            this.includePathBrowseButton.CanFocus = true;
            this.includePathBrowseButton.Name = "includePathBrowseButton";
            this.includePathBrowseButton.UseUnderline = true;
            this.includePathBrowseButton.Label = Mono.Unix.Catalog.GetString("Browse...");
            this.vbox5.Add(this.includePathBrowseButton);
            Gtk.Box.BoxChild w39 = ((Gtk.Box.BoxChild)(this.vbox5[this.includePathBrowseButton]));
            w39.Position = 0;
            w39.Expand = false;
            w39.Fill = false;
            // Container child vbox5.Gtk.Box+BoxChild
            this.includePathRemoveButton = new Gtk.Button();
            this.includePathRemoveButton.Sensitive = false;
            this.includePathRemoveButton.CanFocus = true;
            this.includePathRemoveButton.Name = "includePathRemoveButton";
            this.includePathRemoveButton.UseUnderline = true;
            this.includePathRemoveButton.Label = Mono.Unix.Catalog.GetString("Remove");
            this.vbox5.Add(this.includePathRemoveButton);
            Gtk.Box.BoxChild w40 = ((Gtk.Box.BoxChild)(this.vbox5[this.includePathRemoveButton]));
            w40.Position = 1;
            w40.Expand = false;
            w40.Fill = false;
            this.table3.Add(this.vbox5);
            Gtk.Table.TableChild w41 = ((Gtk.Table.TableChild)(this.table3[this.vbox5]));
            w41.TopAttach = ((uint)(1));
            w41.BottomAttach = ((uint)(2));
            w41.LeftAttach = ((uint)(2));
            w41.RightAttach = ((uint)(3));
            w41.XOptions = ((Gtk.AttachOptions)(4));
            this.vbox7.Add(this.table3);
            Gtk.Box.BoxChild w42 = ((Gtk.Box.BoxChild)(this.vbox7[this.table3]));
            w42.Position = 0;
            this.notebook1.Add(this.vbox7);
            Gtk.Notebook.NotebookChild w43 = ((Gtk.Notebook.NotebookChild)(this.notebook1[this.vbox7]));
            w43.Position = 2;
            // Notebook tab
            this.label3 = new Gtk.Label();
            this.label3.Name = "label3";
            this.label3.LabelProp = Mono.Unix.Catalog.GetString("Paths");
            this.notebook1.SetTabLabel(this.vbox7, this.label3);
            this.label3.ShowAll();
            this.Add(this.notebook1);
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.Show();
            this.targetComboBox.Changed += new System.EventHandler(this.OnTargetComboBoxChanged);
            this.browseButton.Clicked += new System.EventHandler(this.OnBrowseButtonClick);
            this.removeLibButton.Clicked += new System.EventHandler(this.OnRemoveLibButtonClicked);
            this.removeLibButton.Clicked += new System.EventHandler(this.OnLibRemoved);
            this.libTreeView.CursorChanged += new System.EventHandler(this.OnLibTreeViewCursorChanged);
            this.libAddEntry.Changed += new System.EventHandler(this.OnLibAddEntryChanged);
            this.libAddEntry.Activated += new System.EventHandler(this.OnLibAddEntryActivated);
            this.addLibButton.Clicked += new System.EventHandler(this.OnLibAdded);
            this.includePathBrowseButton.Clicked += new System.EventHandler(this.OnIncludePathBrowseButtonClick);
            this.includePathRemoveButton.Clicked += new System.EventHandler(this.OnIncludePathRemoveButtonClicked);
            this.includePathRemoveButton.Clicked += new System.EventHandler(this.OnIncludePathRemoved);
            this.includePathTreeView.CursorChanged += new System.EventHandler(this.OnIncludePathTreeViewCursorChanged);
            this.includePathEntry.Changed += new System.EventHandler(this.OnIncludePathEntryChanged);
            this.includePathEntry.Activated += new System.EventHandler(this.OnIncludePathEntryActivated);
            this.includePathAddButton.Clicked += new System.EventHandler(this.OnIncludePathAdded);
        }
    }
}
