# 
msgid ""
msgstr ""
"Project-Id-Version: MonoDevelop Hungarian translation 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2008-02-03 23:23+0100\n"
"Last-Translator: Gergely Kiss <mail.gery@gmail.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit"

#: ../MonoDevelop.Database.Sql/ConnectionContextService.cs:112
msgid "Unable to load stored SQL connection information."
msgstr "Nem sikerült betölteni a tárolt SQL-kapcsolat adatait."

#: ../MonoDevelop.Database.Sql/QueryService.cs:73
msgid "Please enter the password for connection '{0}'"
msgstr "Kérem, adja meg a kapcsolathoz ('{0}') tartozó jelszót"

#: ../MonoDevelop.Database.Sql/QueryService.cs:75
msgid "Enter Password"
msgstr "Jelszó megadása"

#: ../MonoDevelop.Database.Query/MonoDevelop.Database.Query.addin.xml:31
msgid "Query Database"
msgstr "Adatbázis lekérdezése"

#: ../MonoDevelop.Database.Query/MonoDevelop.Database.Query.addin.xml:37
msgid "_Database"
msgstr "A_datbázis"

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:76
msgid "Execute"
msgstr "Futtatás"

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:79
msgid "Clear Results"
msgstr "Eredmények törlése"

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:109
msgid "Status"
msgstr "Állapot"

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:169
msgid "Unable to connect to database '{0}'"
msgstr "Nem sikerült kapcsolódni az adatbázishoz ({0})."

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:202
msgid "Table"
msgstr "Tábla"

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:203
msgid "Affected Rows"
msgstr "Érintett sorok"

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:216
msgid "No Results"
msgstr "Nincs visszatérési érték"

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:234
msgid "Executing query"
msgstr "Lekérdezés futtatása"

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:248
msgid "Query execute cancelled"
msgstr "A lekérdezés futtatása meg lett szakítva"

#: ../MonoDevelop.Database.Query/SqlQueryView.cs:185
msgid "Query executed ({0} result table)"
msgstr "A lekérdezés sikeresen lefutott ({0} eredménytábla jött létre)"

#: ../MonoDevelop.Database.Sql.Sqlite/SqliteGuiProvider.cs:41
msgid "Save Database"
msgstr "Adatbázis mentése"

#: ../MonoDevelop.Database.Sql.Sqlite/SqliteGuiProvider.cs:47
msgid "Open Database"
msgstr "Adatbázis megnyitása"

#: ../MonoDevelop.Database.Sql.Sqlite/SqliteGuiProvider.cs:60
msgid "SQLite databases"
msgstr "SQLite adatbázisok"

#: ../MonoDevelop.Database.Sql.Sqlite/SqliteGuiProvider.cs:63
#: ../MonoDevelop.Database.Components/Widgets/SqlEditorWidget.cs:110
msgid "All files"
msgstr "Minden fájl"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:27
msgid "Database Browser"
msgstr "Adatbáziskezelő"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:28
msgid "Show System Objects"
msgstr "Rendszerobjektumok megjelenítése"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:166
#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:299
#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.CreateDatabaseDialog.cs:135
msgid "Database"
msgstr "Adatbázis"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:169
msgid "Add Connection"
msgstr "Új kapcsolat"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:172
msgid "Edit Connection"
msgstr "Kapcsolat szerkesztése"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:175
msgid "Remove"
msgstr "Eltávolítás"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:178
msgid "Refresh"
msgstr "Frissítés"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:181
msgid "Connect"
msgstr "Kapcsolódás"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:184
msgid "Disconnect"
msgstr "Kapcsolat bontása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:187
msgid "Select *"
msgstr "Select *"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:190
msgid "Select x,y,..."
msgstr "Select x,y,..."

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:193
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TableNodeBuilder.cs:255
msgid "Empty Table"
msgstr "Üres tábla"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:196
msgid "Query"
msgstr "Lekérdezés"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:199
msgid "Re_name"
msgstr "Át_nevezés"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:207
#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.CreateDatabaseDialog.cs:41
msgid "Create Database"
msgstr "Adatbázis létrehozása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:210
#: ../MonoDevelop.Database.Designer/Dialogs/TableEditorDialog.cs:74
msgid "Create Table"
msgstr "Tábla létrehozása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:213
#: ../MonoDevelop.Database.Designer/Dialogs/ViewEditorDialog.cs:64
msgid "Create View"
msgstr "Nézet létrehozása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:216
#: ../MonoDevelop.Database.Designer/Dialogs/ProcedureEditorDialog.cs:64
msgid "Create Procedure"
msgstr "Eljárás létrehozása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:219
msgid "Create Constraint"
msgstr "Megszorítás létrehozása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:222
msgid "Create Trigger"
msgstr "Trigger létrehozása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:225
#: ../MonoDevelop.Database.Designer/Dialogs/UserEditorDialog.cs:62
msgid "Create User"
msgstr "Új felhasználó"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:229
msgid "Alter Database"
msgstr "Adatbázis módosítása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:232
#: ../MonoDevelop.Database.Designer/Dialogs/TableEditorDialog.cs:76
msgid "Alter Table"
msgstr "Tábla módosítása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:235
#: ../MonoDevelop.Database.Designer/Dialogs/ViewEditorDialog.cs:66
msgid "Alter View"
msgstr "Nézet módosítása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:238
#: ../MonoDevelop.Database.Designer/Dialogs/ProcedureEditorDialog.cs:66
msgid "Alter Procedure"
msgstr "Eljárás módosítása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:241
msgid "Alter Constraint"
msgstr "Megszorítás módosítása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:244
msgid "Alter Trigger"
msgstr "Trigger módosítása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:247
#: ../MonoDevelop.Database.Designer/Dialogs/UserEditorDialog.cs:64
msgid "Alter User"
msgstr "Felhasználó módosítása"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:251
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConnectionContextNodeBuilder.cs:247
msgid "Drop Database"
msgstr "Adatbázis törlése"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:254
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TableNodeBuilder.cs:281
msgid "Drop Table"
msgstr "Tábla törlése"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:257
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ViewNodeBuilder.cs:191
msgid "Drop View"
msgstr "Nézet törlése"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:260
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ProcedureNodeBuilder.cs:190
msgid "Drop Procedure"
msgstr "Eljárás törlése"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:263
msgid "Drop Trigger"
msgstr "Trigger törlése"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:266
msgid "Drop Constraint"
msgstr "Megszorítás törlése"

#: ../MonoDevelop.Database.ConnectionManager/MonoDevelop.Database.ConnectionManager.addin.xml:269
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/UserNodeBuilder.cs:170
msgid "Drop User"
msgstr "Felhasználó törlése"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConnectionContextCollectionNodeBuilder.cs:66
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConnectionContextCollectionNodeBuilder.cs:71
msgid "Database Connections"
msgstr "Adatbázis-kapcsolatok"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConnectionContextNodeBuilder.cs:87
msgid "Unable to connect:"
msgstr "Nem sikerült csatlakozni:"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConnectionContextNodeBuilder.cs:167
msgid "Are you sure you want to remove connection '{0}'?"
msgstr "Biztos benne, hogy törli a kapcsolatot ('{0}')?"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConnectionContextNodeBuilder.cs:168
msgid "Remove Connection"
msgstr "Kapcsolat törlése"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConnectionContextNodeBuilder.cs:246
msgid "Are you sure you want to drop database '{0}'"
msgstr "Biztosan törli az adatbázist ('{0}')?"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TablesNodeBuilder.cs:66
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TablesNodeBuilder.cs:71
msgid "Tables"
msgstr "Táblák"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ViewsNodeBuilder.cs:65
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ViewsNodeBuilder.cs:70
msgid "Views"
msgstr "Nézetek"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ProceduresNodeBuilder.cs:65
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ProceduresNodeBuilder.cs:70
msgid "Procedures"
msgstr "Eljárások"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/UsersNodeBuilder.cs:65
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/UsersNodeBuilder.cs:70
msgid "Users"
msgstr "Felhasználók"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConstraintsNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConstraintsNodeBuilder.cs:68
#: ../MonoDevelop.Database.Designer/Dialogs/TableEditorDialog.cs:90
msgid "Constraints"
msgstr "Megszorítások"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TableNodeBuilder.cs:254
msgid "Are you sure you want to empty table '{0}'"
msgstr "Biztosan törli az összes rekordot a táblában ('{0}')?"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TableNodeBuilder.cs:271
msgid "Table emptied"
msgstr "A rekordok törlése sikerült"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TableNodeBuilder.cs:280
msgid "Are you sure you want to drop table '{0}'"
msgstr "Biztosan törölni szeretné a táblát ('{0}')?"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/UserNodeBuilder.cs:169
msgid "Are you sure you want to drop user '{0}'"
msgstr "Biztosan törölni szeretné a felhasználót ('{0}')?"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ProcedureNodeBuilder.cs:189
msgid "Are you sure you want to drop procedure '{0}'"
msgstr "Biztosan törölni szeretné az eljárást ('{0}')?"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ViewNodeBuilder.cs:190
msgid "Are you sure you want to drop view '{0}'"
msgstr "Biztosan törölni szeretné a nézetet ('{0}')?"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ColumnNodeBuilder.cs:56
#: ../MonoDevelop.Database.Components/Widgets/SelectColumnWidget.cs:69
#: ../MonoDevelop.Database.Designer/Widgets/CheckConstraintEditorWidget.cs:87
msgid "Column"
msgstr "Mező"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ColumnsNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ColumnsNodeBuilder.cs:68
#: ../MonoDevelop.Database.Designer/Dialogs/TableEditorDialog.cs:83
#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.ForeignKeyConstraintEditorWidget.cs:140
msgid "Columns"
msgstr "Mezők"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ConstraintNodeBuilder.cs:56
msgid "Constraint"
msgstr "Megszorítás"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/AggregatesNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/AggregatesNodeBuilder.cs:68
msgid "Aggregates"
msgstr "Összesítések"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/GroupsNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/GroupsNodeBuilder.cs:68
msgid "Groups"
msgstr "Csoportok"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/LanguagesNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/LanguagesNodeBuilder.cs:68
msgid "Languages"
msgstr "Nyelvek"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/OperatorsNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/OperatorsNodeBuilder.cs:68
msgid "Operators"
msgstr "Operátorok"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/RolesNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/RolesNodeBuilder.cs:68
msgid "Roles"
msgstr "Szerepek"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/RulesNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/RulesNodeBuilder.cs:68
msgid "Rules"
msgstr "Szabályok"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/SequencesNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/SequencesNodeBuilder.cs:68
msgid "Sequences"
msgstr "Folyamatok"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TriggersNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TriggersNodeBuilder.cs:68
#: ../MonoDevelop.Database.Designer/Dialogs/TableEditorDialog.cs:100
msgid "Triggers"
msgstr "Triggerek"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TypesNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/TypesNodeBuilder.cs:68
msgid "Types"
msgstr "Típusok"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ParametersNodeBuilder.cs:63
#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ParametersNodeBuilder.cs:68
msgid "Parameters"
msgstr "Paraméterek"

#: ../MonoDevelop.Database.ConnectionManager/NodeBuilders/ParameterNodeBuilder.cs:56
msgid "Parameter"
msgstr "Paraméter"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.SelectColumnDialog.cs:31
msgid "Select Column"
msgstr "Mező kijelölése"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.SelectColumnDialog.cs:55
msgid "Select All"
msgstr "Mindent kijelöl"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.SelectColumnDialog.cs:65
msgid "Deselect All"
msgstr "Kijelölés megszüntetése"

#: ../MonoDevelop.Database.Components/Widgets/ColumnMappingWidget.cs:46
#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:244
#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:93
#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:84
#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.TableEditorDialog.cs:61
#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.CreateDatabaseDialog.cs:124
#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.ViewEditorDialog.cs:56
#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.ProcedureEditorDialog.cs:56
#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.UserEditorDialog.cs:50
#: ../MonoDevelop.Database.Designer/Widgets/ForeignKeyConstraintEditorWidget.cs:113
#: ../MonoDevelop.Database.Designer/Widgets/CheckConstraintEditorWidget.cs:86
#: ../MonoDevelop.Database.Designer/Widgets/PrimaryKeyConstraintEditorWidget.cs:77
#: ../MonoDevelop.Database.Designer/Widgets/UniqueConstraintEditorWidget.cs:82
msgid "Name"
msgstr "Név"

#: ../MonoDevelop.Database.Components/Widgets/ColumnMappingWidget.cs:56
#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:235
#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:94
#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:85
msgid "Type"
msgstr "Típus"

#: ../MonoDevelop.Database.Components/Widgets/ColumnMappingWidget.cs:61
msgid "Property Name"
msgstr "Tulajdonság neve"

#: ../MonoDevelop.Database.Components/Widgets/ColumnMappingWidget.cs:68
msgid "Property Type"
msgstr "Tulajdonság típusa"

#: ../MonoDevelop.Database.Components/Widgets/ColumnMappingWidget.cs:73
#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:87
msgid "Nullable"
msgstr "NULL-értéket felvehet"

#: ../MonoDevelop.Database.Components/Widgets/ColumnMappingWidget.cs:78
msgid "Create Setter"
msgstr "Beállító (setter) létrehozása"

#: ../MonoDevelop.Database.Components/Widgets/ColumnMappingWidget.cs:85
msgid "Ctor Parameter"
msgstr "Ctor paraméter"

# Máris le lett küzdve egy eltérés a magyar és az angol nyelv között. :)
# Sok helyen láttam zavaros megoldásokat az "of" szó fordításakor,
# szerintem így a legkorrektebb.
#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DataGrid.cs:106
msgid "of"
msgstr "/"

#: ../MonoDevelop.Database.Components/Widgets/DataGrid/Renderers/BooleanContentRenderer.cs:43
msgid "Y"
msgstr "I"

#: ../MonoDevelop.Database.Components/Widgets/DataGrid/Renderers/BooleanContentRenderer.cs:43
msgid "N"
msgstr "N"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.ShowImageDialog.cs:31
msgid "Image"
msgstr "Kép"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.ShowImageDialog.cs:75
msgid "Unable to load object as Image"
msgstr "Nem lehet az objektumot képként betölteni"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.ShowTextDialog.cs:23
msgid "Text"
msgstr "Szöveg"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.ShowTextDialog.cs:49
msgid "button462"
msgstr "button462"

#: ../MonoDevelop.Database.Components/Dialogs/DatabaseConnectionSettingsDialog.cs:64
msgid "Edit Database Connection"
msgstr "Adatbázis-kapcsolat szerkesztése"

#: ../MonoDevelop.Database.Components/Dialogs/DatabaseConnectionSettingsDialog.cs:66
msgid "Add Database Connection"
msgstr "Új adatbázis-kapcsolat"

#: ../MonoDevelop.Database.Components/Dialogs/DatabaseConnectionSettingsDialog.cs:121
#: ../MonoDevelop.Database.Components/Dialogs/DatabaseConnectionSettingsDialog.cs:229
#: ../MonoDevelop.Database.Components/Dialogs/DatabaseConnectionSettingsDialog.cs:289
msgid "No databases found!"
msgstr "Nincs egyetlen adatbázis sem."

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:215
msgid "Save Password"
msgstr "Jelszó mentése"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:255
msgid "Server"
msgstr "Kiszolgáló"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:266
msgid "Port"
msgstr "Port"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:277
msgid "Username"
msgstr "Felhasználónév"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:288
msgid "Password"
msgstr "Jelszó"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:328
msgid "General"
msgstr "Általános"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:341
msgid "Use custom connection string"
msgstr "Egyedi kapcsolódási sztring használata"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:354
msgid "Min Pool Size"
msgstr "Legkisebb poolméret"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:363
msgid "Max Pool Size"
msgstr "Legnagyobb poolméret"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.DatabaseConnectionSettingsDialog.cs:426
msgid "Advanced"
msgstr "Fejlett"

#: ../MonoDevelop.Database.Components/Dialogs/WaitDialog.cs:57
msgid "Please Wait"
msgstr "Kérem, várjon..."

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.WaitDialog.cs:29
msgid "WaitDialog"
msgstr "Várakozási ablak"

#: ../MonoDevelop.Database.Components/gtk-gui/MonoDevelop.Database.Components.WaitDialog.cs:63
msgid "Please wait"
msgstr "Kérem, várjon..."

#: ../MonoDevelop.Database.Components/Widgets/SqlEditorWidget.cs:97
#: ../MonoDevelop.Database.Components/MonoDevelop.Database.Components.addin.xml:46
msgid "Import From File"
msgstr "Importálás fájlból"

#: ../MonoDevelop.Database.Components/Widgets/SqlEditorWidget.cs:107
#: ../MonoDevelop.Database.Components/Widgets/SqlEditorWidget.cs:139
msgid "SQL files"
msgstr "SQL fájlok"

#: ../MonoDevelop.Database.Components/Widgets/SqlEditorWidget.cs:128
#: ../MonoDevelop.Database.Components/MonoDevelop.Database.Components.addin.xml:49
msgid "Export To File"
msgstr "Exportálás fájlba"

#: ../MonoDevelop.Database.Components/Widgets/SqlEditorWidget.cs:145
msgid "Are you sure you want to overwrite the file '{0}'?"
msgstr "Biztos, hogy felülírja a fájlt ('{0}')?"

#: ../MonoDevelop.Database.Components/Widgets/SqlEditorWidget.cs:146
msgid "Overwrite?"
msgstr "Felül szeretné írni?"

#: ../MonoDevelop.Database.Components/MonoDevelop.Database.Components.addin.xml:37
msgid "Display content as text"
msgstr "Tartalom megjelenítése szövegként"

#: ../MonoDevelop.Database.Components/MonoDevelop.Database.Components.addin.xml:39
msgid "Display content as xml text"
msgstr "Tartalom megjelenítése XML kódként"

#: ../MonoDevelop.Database.Components/MonoDevelop.Database.Components.addin.xml:41
msgid "Display content as xml tree"
msgstr "Tartalom megjelenítése XML faként"

#: ../MonoDevelop.Database.Components/MonoDevelop.Database.Components.addin.xml:43
msgid "Display content as image"
msgstr "Tartalom megjelenítése képként"

#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:95
msgid "Event"
msgstr "Esemény"

#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:96
msgid "Each Row"
msgstr "Minden rekord"

#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:97
msgid "Position"
msgstr "Pozíció"

#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:98
msgid "Active"
msgstr "Aktív"

#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:99
#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:88
#: ../MonoDevelop.Database.Designer/Dialogs/TableEditorDialog.cs:105
#: ../MonoDevelop.Database.Designer/Dialogs/ViewEditorDialog.cs:77
#: ../MonoDevelop.Database.Designer/Dialogs/ProcedureEditorDialog.cs:77
msgid "Comment"
msgstr "Megjegyzés"

#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:185
msgid "Are you sure you want to remove trigger '{0}'?"
msgstr "Biztosan törli a triggert ('{0}')?"

#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:186
msgid "Remove Trigger"
msgstr "Trigger törlése"

#: ../MonoDevelop.Database.Designer/Widgets/TriggersEditorWidget.cs:341
msgid "Trigger '{0}' does not contain a trigger statement."
msgstr "A trigger ('{0}') nem tartalmazza a TRIGGER kulcsszót."

#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.TriggersEditorWidget.cs:123
msgid "Statement"
msgstr "Állítás"

#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:83
msgid "PK"
msgstr "EK"

#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:86
msgid "Length"
msgstr "Szélesség"

#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:220
msgid "Are you sure you want to remove column '{0}'"
msgstr "Biztosan törli a mezőt ('{0}')?"

#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:221
msgid "Remove Column"
msgstr "Mező törlése"

#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:384
msgid "Unknown data type '{0}' applied to column '{1}'."
msgstr "Ismeretlen adattípust ('{0}') adott meg a mezőhöz ('{1}')."

#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:396
msgid "Table '{0}' must contain at least one primary key."
msgstr ""
"A táblához ('{0}') hozzá kell rendelni legalább egy elsődleges kulcsot."

#: ../MonoDevelop.Database.Designer/Widgets/ColumnsEditorWidget.cs:403
msgid "Table '{0}' must contain at least 1 column."
msgstr "A tábla ('{0}') nem tartalmaz egyetlen mezőt sem."

#: ../MonoDevelop.Database.Designer/Widgets/ConstraintsEditorWidget.cs:84
msgid "Primary Key"
msgstr "Elsődleges kulcs"

#: ../MonoDevelop.Database.Designer/Widgets/ConstraintsEditorWidget.cs:91
msgid "Foreign Key"
msgstr "Idegenkulcs"

#: ../MonoDevelop.Database.Designer/Widgets/ConstraintsEditorWidget.cs:98
msgid "Check"
msgstr "Ellenőrzés"

#: ../MonoDevelop.Database.Designer/Widgets/ConstraintsEditorWidget.cs:105
msgid "Unique"
msgstr "Egyedi"

#: ../MonoDevelop.Database.Designer/Dialogs/TableEditorDialog.cs:227
msgid "No name specified."
msgstr "Nem adott meg nevet."

#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.TableEditorDialog.cs:39
msgid "Edit Table"
msgstr "Tábla szerkesztése"

#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.TableEditorDialog.cs:112
msgid "Preview SQL"
msgstr "SQL-kód előnézete"

#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.CreateDatabaseDialog.cs:146
msgid "Base Settings"
msgstr "Alapbeállítások"

#: ../MonoDevelop.Database.Designer/Dialogs/ViewEditorDialog.cs:72
#: ../MonoDevelop.Database.Designer/Dialogs/ProcedureEditorDialog.cs:72
msgid "Definition"
msgstr "Definíció"

#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.PreviewDialog.cs:27
msgid "Preview"
msgstr "Előnézet"

#: ../MonoDevelop.Database.Designer/Widgets/ForeignKeyConstraintEditorWidget.cs:114
msgid "Reference Table"
msgstr "Referenciatábla"

#: ../MonoDevelop.Database.Designer/Widgets/ForeignKeyConstraintEditorWidget.cs:115
#: ../MonoDevelop.Database.Designer/Widgets/CheckConstraintEditorWidget.cs:88
#: ../MonoDevelop.Database.Designer/Widgets/UniqueConstraintEditorWidget.cs:83
msgid "Column Constraint"
msgstr "Mezőmegszorítás"

#: ../MonoDevelop.Database.Designer/Widgets/ForeignKeyConstraintEditorWidget.cs:116
msgid "Delete Action"
msgstr "Akció törlése"

#: ../MonoDevelop.Database.Designer/Widgets/ForeignKeyConstraintEditorWidget.cs:117
msgid "Update Action"
msgstr "Akció frissítése"

#: ../MonoDevelop.Database.Designer/Widgets/ForeignKeyConstraintEditorWidget.cs:191
#: ../MonoDevelop.Database.Designer/Widgets/CheckConstraintEditorWidget.cs:153
#: ../MonoDevelop.Database.Designer/Widgets/PrimaryKeyConstraintEditorWidget.cs:169
#: ../MonoDevelop.Database.Designer/Widgets/UniqueConstraintEditorWidget.cs:188
msgid "Are you sure you want to remove constraint '{0}'?"
msgstr "Biztosan törli a megszorítást ('{0}')?"

#: ../MonoDevelop.Database.Designer/Widgets/ForeignKeyConstraintEditorWidget.cs:192
#: ../MonoDevelop.Database.Designer/Widgets/CheckConstraintEditorWidget.cs:154
#: ../MonoDevelop.Database.Designer/Widgets/PrimaryKeyConstraintEditorWidget.cs:170
#: ../MonoDevelop.Database.Designer/Widgets/UniqueConstraintEditorWidget.cs:189
msgid "Remove Constraint"
msgstr "Megszorítás törlése"

#: ../MonoDevelop.Database.Designer/Widgets/ForeignKeyConstraintEditorWidget.cs:346
#: ../MonoDevelop.Database.Designer/Widgets/UniqueConstraintEditorWidget.cs:228
msgid "Unique Key constraint '{0}' must be applied to one or more columns."
msgstr ""
"Az egyedi kulcs megszorítást ('{0}') legalább egy mezőre alkalmazni kell."

#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.ForeignKeyConstraintEditorWidget.cs:168
msgid "Reference Columns"
msgstr "Referenciamezők"

#: ../MonoDevelop.Database.Designer/Widgets/CheckConstraintEditorWidget.cs:250
msgid "Checked constraint '{0}' does not contain a check statement."
msgstr ""
"Az ellenőrzött megszorításban ('{0}') szerepelnie kell a CHECK kulcsszónak."

#: ../MonoDevelop.Database.Designer/Widgets/CheckConstraintEditorWidget.cs:255
msgid ""
"Checked constraint '{0}' is marked as a column constraint but is not applied "
"to a column."
msgstr ""
"Az ellenőrzött megszorítás ('{0}') mezőre vonatkozó megszorításnak van "
"megjelölve, de nincs alkalmazva egyetlen mezőre sem."

#: ../MonoDevelop.Database.Designer/gtk-gui/MonoDevelop.Database.Designer.CheckConstraintEditorWidget.cs:122
msgid "Check Condition"
msgstr "Feltételek ellenőrzése"

#: ../MonoDevelop.Database.Designer/Widgets/PrimaryKeyConstraintEditorWidget.cs:193
msgid "Primary Key constraint '{0}' must be applied to one or more columns."
msgstr ""
"Az elsődleges kulcs megszorítást ('{0}') alkalmazni kell legalább egy mezőre."

