// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace MonoDevelop.Database.Sql.SqlServer {
    
    
    public partial class SqlServerCreateDatabaseWidget {
        
        private Gtk.Table table;
        
        private Gtk.ComboBoxEntry comboCollation;
        
        private Gtk.Entry entryName;
        
        private Gtk.Label label1;
        
        private Gtk.Label label3;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget MonoDevelop.Database.Sql.SqlServer.SqlServerCreateDatabaseWidget
            Stetic.BinContainer.Attach(this);
            this.Name = "MonoDevelop.Database.Sql.SqlServer.SqlServerCreateDatabaseWidget";
            // Container child MonoDevelop.Database.Sql.SqlServer.SqlServerCreateDatabaseWidget.Gtk.Container+ContainerChild
            this.table = new Gtk.Table(((uint)(2)), ((uint)(2)), false);
            this.table.Name = "table";
            this.table.RowSpacing = ((uint)(6));
            this.table.ColumnSpacing = ((uint)(6));
            this.table.BorderWidth = ((uint)(6));
            // Container child table.Gtk.Table+TableChild
            this.comboCollation = Gtk.ComboBoxEntry.NewText();
            this.comboCollation.Name = "comboCollation";
            this.table.Add(this.comboCollation);
            Gtk.Table.TableChild w1 = ((Gtk.Table.TableChild)(this.table[this.comboCollation]));
            w1.TopAttach = ((uint)(1));
            w1.BottomAttach = ((uint)(2));
            w1.LeftAttach = ((uint)(1));
            w1.RightAttach = ((uint)(2));
            w1.XOptions = ((Gtk.AttachOptions)(4));
            w1.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table.Gtk.Table+TableChild
            this.entryName = new Gtk.Entry();
            this.entryName.CanFocus = true;
            this.entryName.Name = "entryName";
            this.entryName.IsEditable = true;
            this.entryName.InvisibleChar = '●';
            this.table.Add(this.entryName);
            Gtk.Table.TableChild w2 = ((Gtk.Table.TableChild)(this.table[this.entryName]));
            w2.LeftAttach = ((uint)(1));
            w2.RightAttach = ((uint)(2));
            w2.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table.Gtk.Table+TableChild
            this.label1 = new Gtk.Label();
            this.label1.Name = "label1";
            this.label1.Xalign = 0F;
            this.label1.LabelProp = AddinCatalog.GetString("Database:");
            this.table.Add(this.label1);
            Gtk.Table.TableChild w3 = ((Gtk.Table.TableChild)(this.table[this.label1]));
            w3.XOptions = ((Gtk.AttachOptions)(4));
            w3.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table.Gtk.Table+TableChild
            this.label3 = new Gtk.Label();
            this.label3.Name = "label3";
            this.label3.Xalign = 0F;
            this.label3.LabelProp = AddinCatalog.GetString("Collation:");
            this.table.Add(this.label3);
            Gtk.Table.TableChild w4 = ((Gtk.Table.TableChild)(this.table[this.label3]));
            w4.TopAttach = ((uint)(1));
            w4.BottomAttach = ((uint)(2));
            w4.XOptions = ((Gtk.AttachOptions)(4));
            w4.YOptions = ((Gtk.AttachOptions)(4));
            this.Add(this.table);
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.Show();
        }
    }
}
