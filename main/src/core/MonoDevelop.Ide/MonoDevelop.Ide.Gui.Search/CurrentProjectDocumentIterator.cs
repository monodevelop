// Permission is hereby granted, free of charge, to any person obtaining 
// a copy of this software and associated documentation files (the 
// "Software"), to deal in the Software without restriction, including 
// without limitation the rights to use, copy, modify, merge, publish, 
// distribute, sublicense, and/or sell copies of the Software, and to 
// permit persons to whom the Software is furnished to do so, subject to 
// the following conditions: 
//  
// The above copyright notice and this permission notice shall be 
// included in all copies or substantial portions of the Software. 
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// 
// Copyright (c) 2008 Novell, Inc. (http://www.novell.com) 
// 
// Authors: 
//      Andres G. Aragoneses <aaragoneses@novell.com>
// 

using System;
using System.Collections;

using MonoDevelop.Projects;
using MonoDevelop.Core;

namespace MonoDevelop.Ide.Gui.Search
{
	internal class CurrentProjectDocumentIterator : FilesDocumentIterator
	{
		string projectName;

		public CurrentProjectDocumentIterator () : base ()
		{
		}
		
		public override string GetSearchDescription (string pattern)
		{
			return GettextCatalog.GetString ("Looking for '{0}' in project '{1}'", pattern, projectName);
		}
		
		public override string GetReplaceDescription (string pattern)
		{
			return GettextCatalog.GetString ("Replacing '{0}' in project '{1}'", pattern, projectName);
		}
		
		public override void Reset() 
		{
			files.Clear();
			Document document;
			if (IdeApp.Workspace.IsOpen && 
			   ((document = IdeApp.Workbench.ActiveDocument) != null) &&
			   //FIXME: when document.FileName == null, maybe it's interesting to pick the file selected on the project pad...
			   (document.FileName != null)) {
				Project theProject = IdeApp.Workspace.GetProjectContainingFile (document.FileName);
				projectName = theProject.Name;
				foreach (ProjectFile file in theProject.Files)
					if (file.Subtype == Subtype.Code)
						files.Add(file.Name);
			} else {
				//FIXME: it may be interesting to show a warning dialog/message to the user in this case
				projectName = GettextCatalog.GetString ("(none selected)");
			}
			
			curIndex = -1;
		}
	}
}
