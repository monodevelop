// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace MonoDevelop.Ide.ExternalTools {
    
    
    public partial class ExternalToolPanelWidget {
        
        private Gtk.VBox vbox32;
        
        private Gtk.HBox hbox21;
        
        private Gtk.ScrolledWindow scrolledwindow4;
        
        private Gtk.TreeView toolListBox;
        
        private Gtk.VBox buttons;
        
        private Gtk.Button addButton;
        
        private Gtk.Button removeButton;
        
        private Gtk.Label label34;
        
        private Gtk.Button moveUpButton;
        
        private Gtk.Button moveDownButton;
        
        private Gtk.Table table2;
        
        private Gtk.Label argumentLabel;
        
        private MonoDevelop.Components.FileEntry browseButton;
        
        private Gtk.Label commandLabel;
        
        private Gtk.Table table3;
        
        private Gtk.Button argumentQuickInsertButton;
        
        private Gtk.Entry argumentTextBox;
        
        private Gtk.Table table4;
        
        private Gtk.Button workingDirQuickInsertButton;
        
        private Gtk.Entry workingDirTextBox;
        
        private Gtk.Label titleLabel;
        
        private Gtk.Entry titleTextBox;
        
        private Gtk.Label workingDirLabel;
        
        private Gtk.Table table1;
        
        private Gtk.CheckButton promptArgsCheckBox;
        
        private Gtk.CheckButton saveCurrentFileCheckBox;
        
        private Gtk.CheckButton useOutputPadCheckBox;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget MonoDevelop.Ide.ExternalTools.ExternalToolPanelWidget
            Stetic.BinContainer.Attach(this);
            this.Name = "MonoDevelop.Ide.ExternalTools.ExternalToolPanelWidget";
            // Container child MonoDevelop.Ide.ExternalTools.ExternalToolPanelWidget.Gtk.Container+ContainerChild
            this.vbox32 = new Gtk.VBox();
            this.vbox32.Name = "vbox32";
            this.vbox32.Spacing = 12;
            // Container child vbox32.Gtk.Box+BoxChild
            this.hbox21 = new Gtk.HBox();
            this.hbox21.Name = "hbox21";
            this.hbox21.Spacing = 6;
            // Container child hbox21.Gtk.Box+BoxChild
            this.scrolledwindow4 = new Gtk.ScrolledWindow();
            this.scrolledwindow4.Name = "scrolledwindow4";
            this.scrolledwindow4.ShadowType = ((Gtk.ShadowType)(4));
            // Container child scrolledwindow4.Gtk.Container+ContainerChild
            this.toolListBox = new Gtk.TreeView();
            this.toolListBox.WidthRequest = 200;
            this.toolListBox.HeightRequest = 150;
            this.toolListBox.Name = "toolListBox";
            this.scrolledwindow4.Add(this.toolListBox);
            this.hbox21.Add(this.scrolledwindow4);
            Gtk.Box.BoxChild w2 = ((Gtk.Box.BoxChild)(this.hbox21[this.scrolledwindow4]));
            w2.Position = 0;
            // Container child hbox21.Gtk.Box+BoxChild
            this.buttons = new Gtk.VBox();
            this.buttons.Name = "buttons";
            this.buttons.Spacing = 6;
            // Container child buttons.Gtk.Box+BoxChild
            this.addButton = new Gtk.Button();
            this.addButton.Name = "addButton";
            this.addButton.UseStock = true;
            this.addButton.UseUnderline = true;
            this.addButton.Label = "gtk-add";
            this.buttons.Add(this.addButton);
            Gtk.Box.BoxChild w3 = ((Gtk.Box.BoxChild)(this.buttons[this.addButton]));
            w3.Position = 0;
            w3.Expand = false;
            w3.Fill = false;
            // Container child buttons.Gtk.Box+BoxChild
            this.removeButton = new Gtk.Button();
            this.removeButton.Name = "removeButton";
            this.removeButton.UseStock = true;
            this.removeButton.UseUnderline = true;
            this.removeButton.Label = "gtk-remove";
            this.buttons.Add(this.removeButton);
            Gtk.Box.BoxChild w4 = ((Gtk.Box.BoxChild)(this.buttons[this.removeButton]));
            w4.Position = 1;
            w4.Expand = false;
            w4.Fill = false;
            // Container child buttons.Gtk.Box+BoxChild
            this.label34 = new Gtk.Label();
            this.label34.Name = "label34";
            this.label34.Xalign = 0F;
            this.label34.Yalign = 0F;
            this.label34.LabelProp = "    ";
            this.buttons.Add(this.label34);
            Gtk.Box.BoxChild w5 = ((Gtk.Box.BoxChild)(this.buttons[this.label34]));
            w5.Position = 2;
            // Container child buttons.Gtk.Box+BoxChild
            this.moveUpButton = new Gtk.Button();
            this.moveUpButton.Name = "moveUpButton";
            this.moveUpButton.UseStock = true;
            this.moveUpButton.UseUnderline = true;
            this.moveUpButton.Label = "gtk-go-up";
            this.buttons.Add(this.moveUpButton);
            Gtk.Box.BoxChild w6 = ((Gtk.Box.BoxChild)(this.buttons[this.moveUpButton]));
            w6.Position = 3;
            w6.Expand = false;
            w6.Fill = false;
            // Container child buttons.Gtk.Box+BoxChild
            this.moveDownButton = new Gtk.Button();
            this.moveDownButton.Name = "moveDownButton";
            this.moveDownButton.UseStock = true;
            this.moveDownButton.UseUnderline = true;
            this.moveDownButton.Label = "gtk-go-down";
            this.buttons.Add(this.moveDownButton);
            Gtk.Box.BoxChild w7 = ((Gtk.Box.BoxChild)(this.buttons[this.moveDownButton]));
            w7.Position = 4;
            w7.Expand = false;
            w7.Fill = false;
            this.hbox21.Add(this.buttons);
            Gtk.Box.BoxChild w8 = ((Gtk.Box.BoxChild)(this.hbox21[this.buttons]));
            w8.Position = 1;
            w8.Expand = false;
            w8.Fill = false;
            this.vbox32.Add(this.hbox21);
            Gtk.Box.BoxChild w9 = ((Gtk.Box.BoxChild)(this.vbox32[this.hbox21]));
            w9.Position = 0;
            // Container child vbox32.Gtk.Box+BoxChild
            this.table2 = new Gtk.Table(((uint)(4)), ((uint)(2)), false);
            this.table2.Name = "table2";
            this.table2.RowSpacing = ((uint)(6));
            this.table2.ColumnSpacing = ((uint)(6));
            // Container child table2.Gtk.Table+TableChild
            this.argumentLabel = new Gtk.Label();
            this.argumentLabel.Name = "argumentLabel";
            this.argumentLabel.Xalign = 0F;
            this.argumentLabel.Yalign = 0F;
            this.argumentLabel.LabelProp = Mono.Unix.Catalog.GetString("_Arguments:");
            this.argumentLabel.UseUnderline = true;
            this.table2.Add(this.argumentLabel);
            Gtk.Table.TableChild w10 = ((Gtk.Table.TableChild)(this.table2[this.argumentLabel]));
            w10.TopAttach = ((uint)(2));
            w10.BottomAttach = ((uint)(3));
            w10.XOptions = ((Gtk.AttachOptions)(4));
            w10.YOptions = ((Gtk.AttachOptions)(0));
            // Container child table2.Gtk.Table+TableChild
            this.browseButton = new MonoDevelop.Components.FileEntry();
            this.browseButton.Name = "browseButton";
            this.table2.Add(this.browseButton);
            Gtk.Table.TableChild w11 = ((Gtk.Table.TableChild)(this.table2[this.browseButton]));
            w11.TopAttach = ((uint)(1));
            w11.BottomAttach = ((uint)(2));
            w11.LeftAttach = ((uint)(1));
            w11.RightAttach = ((uint)(2));
            w11.XOptions = ((Gtk.AttachOptions)(4));
            w11.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table2.Gtk.Table+TableChild
            this.commandLabel = new Gtk.Label();
            this.commandLabel.Name = "commandLabel";
            this.commandLabel.Xalign = 0F;
            this.commandLabel.Yalign = 0F;
            this.commandLabel.LabelProp = Mono.Unix.Catalog.GetString("_Command:");
            this.commandLabel.UseUnderline = true;
            this.table2.Add(this.commandLabel);
            Gtk.Table.TableChild w12 = ((Gtk.Table.TableChild)(this.table2[this.commandLabel]));
            w12.TopAttach = ((uint)(1));
            w12.BottomAttach = ((uint)(2));
            w12.XOptions = ((Gtk.AttachOptions)(4));
            w12.YOptions = ((Gtk.AttachOptions)(0));
            // Container child table2.Gtk.Table+TableChild
            this.table3 = new Gtk.Table(((uint)(1)), ((uint)(2)), false);
            this.table3.Name = "table3";
            this.table3.RowSpacing = ((uint)(6));
            this.table3.ColumnSpacing = ((uint)(4));
            // Container child table3.Gtk.Table+TableChild
            this.argumentQuickInsertButton = new Gtk.Button();
            this.argumentQuickInsertButton.Name = "argumentQuickInsertButton";
            this.argumentQuickInsertButton.UseUnderline = true;
            this.argumentQuickInsertButton.Label = " > ";
            this.table3.Add(this.argumentQuickInsertButton);
            Gtk.Table.TableChild w13 = ((Gtk.Table.TableChild)(this.table3[this.argumentQuickInsertButton]));
            w13.LeftAttach = ((uint)(1));
            w13.RightAttach = ((uint)(2));
            w13.XOptions = ((Gtk.AttachOptions)(0));
            w13.YOptions = ((Gtk.AttachOptions)(0));
            // Container child table3.Gtk.Table+TableChild
            this.argumentTextBox = new Gtk.Entry();
            this.argumentTextBox.Name = "argumentTextBox";
            this.argumentTextBox.IsEditable = true;
            this.argumentTextBox.InvisibleChar = '●';
            this.table3.Add(this.argumentTextBox);
            Gtk.Table.TableChild w14 = ((Gtk.Table.TableChild)(this.table3[this.argumentTextBox]));
            w14.YOptions = ((Gtk.AttachOptions)(0));
            this.table2.Add(this.table3);
            Gtk.Table.TableChild w15 = ((Gtk.Table.TableChild)(this.table2[this.table3]));
            w15.TopAttach = ((uint)(2));
            w15.BottomAttach = ((uint)(3));
            w15.LeftAttach = ((uint)(1));
            w15.RightAttach = ((uint)(2));
            w15.YOptions = ((Gtk.AttachOptions)(0));
            // Container child table2.Gtk.Table+TableChild
            this.table4 = new Gtk.Table(((uint)(1)), ((uint)(2)), false);
            this.table4.Name = "table4";
            this.table4.RowSpacing = ((uint)(6));
            this.table4.ColumnSpacing = ((uint)(4));
            // Container child table4.Gtk.Table+TableChild
            this.workingDirQuickInsertButton = new Gtk.Button();
            this.workingDirQuickInsertButton.Name = "workingDirQuickInsertButton";
            this.workingDirQuickInsertButton.UseUnderline = true;
            this.workingDirQuickInsertButton.Label = " > ";
            this.table4.Add(this.workingDirQuickInsertButton);
            Gtk.Table.TableChild w16 = ((Gtk.Table.TableChild)(this.table4[this.workingDirQuickInsertButton]));
            w16.LeftAttach = ((uint)(1));
            w16.RightAttach = ((uint)(2));
            w16.XOptions = ((Gtk.AttachOptions)(0));
            w16.YOptions = ((Gtk.AttachOptions)(0));
            // Container child table4.Gtk.Table+TableChild
            this.workingDirTextBox = new Gtk.Entry();
            this.workingDirTextBox.Name = "workingDirTextBox";
            this.workingDirTextBox.IsEditable = true;
            this.workingDirTextBox.InvisibleChar = '●';
            this.table4.Add(this.workingDirTextBox);
            Gtk.Table.TableChild w17 = ((Gtk.Table.TableChild)(this.table4[this.workingDirTextBox]));
            w17.YOptions = ((Gtk.AttachOptions)(0));
            this.table2.Add(this.table4);
            Gtk.Table.TableChild w18 = ((Gtk.Table.TableChild)(this.table2[this.table4]));
            w18.TopAttach = ((uint)(3));
            w18.BottomAttach = ((uint)(4));
            w18.LeftAttach = ((uint)(1));
            w18.RightAttach = ((uint)(2));
            w18.YOptions = ((Gtk.AttachOptions)(0));
            // Container child table2.Gtk.Table+TableChild
            this.titleLabel = new Gtk.Label();
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Xalign = 0F;
            this.titleLabel.Yalign = 0F;
            this.titleLabel.LabelProp = Mono.Unix.Catalog.GetString("_Title:");
            this.titleLabel.UseUnderline = true;
            this.table2.Add(this.titleLabel);
            Gtk.Table.TableChild w19 = ((Gtk.Table.TableChild)(this.table2[this.titleLabel]));
            w19.XOptions = ((Gtk.AttachOptions)(4));
            w19.YOptions = ((Gtk.AttachOptions)(0));
            // Container child table2.Gtk.Table+TableChild
            this.titleTextBox = new Gtk.Entry();
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.IsEditable = true;
            this.titleTextBox.InvisibleChar = '●';
            this.table2.Add(this.titleTextBox);
            Gtk.Table.TableChild w20 = ((Gtk.Table.TableChild)(this.table2[this.titleTextBox]));
            w20.LeftAttach = ((uint)(1));
            w20.RightAttach = ((uint)(2));
            w20.YOptions = ((Gtk.AttachOptions)(0));
            // Container child table2.Gtk.Table+TableChild
            this.workingDirLabel = new Gtk.Label();
            this.workingDirLabel.Name = "workingDirLabel";
            this.workingDirLabel.Xalign = 0F;
            this.workingDirLabel.Yalign = 0F;
            this.workingDirLabel.LabelProp = Mono.Unix.Catalog.GetString("_Working directory:");
            this.workingDirLabel.UseUnderline = true;
            this.table2.Add(this.workingDirLabel);
            Gtk.Table.TableChild w21 = ((Gtk.Table.TableChild)(this.table2[this.workingDirLabel]));
            w21.TopAttach = ((uint)(3));
            w21.BottomAttach = ((uint)(4));
            w21.XOptions = ((Gtk.AttachOptions)(4));
            w21.YOptions = ((Gtk.AttachOptions)(0));
            this.vbox32.Add(this.table2);
            Gtk.Box.BoxChild w22 = ((Gtk.Box.BoxChild)(this.vbox32[this.table2]));
            w22.Position = 1;
            w22.Expand = false;
            w22.Fill = false;
            // Container child vbox32.Gtk.Box+BoxChild
            this.table1 = new Gtk.Table(((uint)(2)), ((uint)(2)), false);
            this.table1.Name = "table1";
            this.table1.RowSpacing = ((uint)(6));
            this.table1.ColumnSpacing = ((uint)(6));
            // Container child table1.Gtk.Table+TableChild
            this.promptArgsCheckBox = new Gtk.CheckButton();
            this.promptArgsCheckBox.Name = "promptArgsCheckBox";
            this.promptArgsCheckBox.Label = Mono.Unix.Catalog.GetString("_Prompt for arguments");
            this.promptArgsCheckBox.DrawIndicator = true;
            this.promptArgsCheckBox.UseUnderline = true;
            this.table1.Add(this.promptArgsCheckBox);
            Gtk.Table.TableChild w23 = ((Gtk.Table.TableChild)(this.table1[this.promptArgsCheckBox]));
            w23.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.saveCurrentFileCheckBox = new Gtk.CheckButton();
            this.saveCurrentFileCheckBox.CanFocus = true;
            this.saveCurrentFileCheckBox.Name = "saveCurrentFileCheckBox";
            this.saveCurrentFileCheckBox.Label = Mono.Unix.Catalog.GetString("_Save current file");
            this.saveCurrentFileCheckBox.DrawIndicator = true;
            this.saveCurrentFileCheckBox.UseUnderline = true;
            this.table1.Add(this.saveCurrentFileCheckBox);
            Gtk.Table.TableChild w24 = ((Gtk.Table.TableChild)(this.table1[this.saveCurrentFileCheckBox]));
            w24.LeftAttach = ((uint)(1));
            w24.RightAttach = ((uint)(2));
            w24.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.useOutputPadCheckBox = new Gtk.CheckButton();
            this.useOutputPadCheckBox.Name = "useOutputPadCheckBox";
            this.useOutputPadCheckBox.Label = Mono.Unix.Catalog.GetString("Use _output window");
            this.useOutputPadCheckBox.DrawIndicator = true;
            this.useOutputPadCheckBox.UseUnderline = true;
            this.table1.Add(this.useOutputPadCheckBox);
            Gtk.Table.TableChild w25 = ((Gtk.Table.TableChild)(this.table1[this.useOutputPadCheckBox]));
            w25.TopAttach = ((uint)(1));
            w25.BottomAttach = ((uint)(2));
            w25.YOptions = ((Gtk.AttachOptions)(4));
            this.vbox32.Add(this.table1);
            Gtk.Box.BoxChild w26 = ((Gtk.Box.BoxChild)(this.vbox32[this.table1]));
            w26.Position = 2;
            w26.Expand = false;
            w26.Fill = false;
            this.Add(this.vbox32);
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.titleLabel.MnemonicWidget = this.titleTextBox;
            this.Show();
        }
    }
}
