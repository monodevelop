//  IXmlConvertable.cs
//
//  This file was derived from a file from #Develop. 
//
//  Copyright (C) 2001-2007 Mike Krüger <mkrueger@novell.com>
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System.Xml;

namespace MonoDevelop.Core.Properties
{
	/*
	/// <summary>
	/// If you want define own, complex options you can implement this interface
	/// and save it in the main Option class, your class will be saved as xml in
	/// the global properties.
	/// Use your class like any other property. (the conversion will be transparent)
	/// </summary>
	public interface IProperties
	{
		/// <summary>
		/// Converts a <code>XmlElement</code> to an <code>IProperties</code>
		/// </summary>
		/// <returns>
		/// A new <code>IProperties</code> object 
		/// </returns>
		object FromXmlElement(XmlElement element);
		
		/// <summary>
		/// Converts the <code>IProperties</code> object to a <code>XmlElement</code>
		/// </summary>
		/// <returns>
		/// A new <code>XmlElement</code> object which represents the state
		/// of the <code>IProperties</code> object.
		/// </returns>
		XmlElement ToXmlElement(XmlDocument doc);
	}*/
}
