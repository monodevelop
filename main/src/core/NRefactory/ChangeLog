2009-02-24  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Parser/CSharp/cs.ATG:
	* Project/Src/Parser/CSharp/Parser.cs:
	* Project/Src/Parser/CSharp/CSharpParser.cs: Added support for
	  the __arglist keyword.

2009-02-11  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Visitors/CodeDOMOutputVisitor.cs: fixed
	  exception reported in "Bug 473894 - MD does not compile MEF
	  p4".

2009-02-11  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Visitors/CodeDOMOutputVisitor.cs: fixed the
	  exception from "Bug 473894 - MD does not compile MEF p4"

2009-02-07  Michael Hutchinson  <mhutchinson@novell.com>

	* NRefactory.csproj: Flush MD's removal of newline at end of
	  file that was introduced by manually editing with gedit.

2009-02-07  Michael Hutchinson  <mhutchinson@novell.com>

	* NRefactory.csproj: Remove invalid ApplicationIcon value that
	  broke the build in VS.

2009-02-07  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Lexer/LookupTable.cs:
	* Project/Src/OperatorPrecedence.cs:
	* Project/Src/Parser/VBNet/VBNetParser.cs:
	* Project/Src/Parser/CSharp/CSharpParser.cs:
	* Project/Src/Visitors/ToVBNetConvertVisitor.cs:
	* Project/Src/PrettyPrinter/CSharp/OutputFormatter.cs: Fixed
	  compiler warnings.

2009-02-06  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Parser/CSharp/cs.ATG:
	* Project/Src/Parser/CSharp/Parser.cs: fixed "Bug 463850 - MD
	  C# parser issue".

2009-02-06  Lluis Sanchez Gual  <lluis@novell.com>

	* NRefactory.mdp: Remove unused files.

2009-02-06  Lluis Sanchez Gual  <lluis@novell.com>

	* NRefactory.csproj: Migrated to MSBuild file format.

2009-01-26  Michael Hutchinson  <mhutchinson@novell.com>

	* NRefactory.mdp: Flush project format changes.

2009-01-09  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Parser/CSharp/cs.ATG:
	* Project/Src/Parser/CSharp/Parser.cs: Corrected parameter position.

2009-01-09  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Parser/CSharp/cs.ATG:
	* Project/Src/Parser/CSharp/Parser.cs: Corrected destructor start
	location.

2008-12-04  Lluis Sanchez Gual  <lluis@novell.com>

	* Makefile.am: Make it work for parallel build.

2008-11-18  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Lexer/CSharp/ConditionalCompilation.cs: made conditional
	compilation class public.

2008-10-30  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Parser/CSharp/cs.ATG:
	* Project/Src/Parser/CSharp/Parser.cs:
	* Project/Src/Parser/CSharp/CSharpParser.cs: Changed the grammar
	slightly.

2008-10-30  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Parser/IParser.cs:
	* Project/Src/Parser/AbstractParser.cs:
	* Project/Src/Parser/VBNet/VBNetParser.cs:
	* Project/Src/Parser/CSharp/CSharpParser.cs: Added pase type reference.

2008-10-30  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Parser/CSharp/CSharpParser.cs: fixed bug in nrefactory.

2008-10-30  Mike Krüger  <mkrueger@novell.com>

	* Project/Src/Parser/CSharp/CSharpParser.cs: fixed 'Bug 434705 - No
	autocomplete offered if not assigning result of 'new' to a variable'.

2008-10-07  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Parser/CSharp/CSharpParser.cs: fixed 'Bug 431183 -
	  Invalid UnaryExpr'.

2008-09-23  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Parser/CSharp/cs.ATG,
	  Project/Src/Parser/CSharp/Parser.cs: fixed Bug 384199 - c# code
	  parsing breaks on unsafe pointers

2008-09-23  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs,
	  Project/Src/Lexer/Special/PreProcessingDirective.cs,
	  Project/Src/Lexer/AbstractLexer.cs,
	  Project/Src/PrettyPrinter/CSharp/CSharpOutputVisitor.cs,
	  Project/Src/PrettyPrinter/CSharp/OutputFormatter.cs,
	  Project/Src/PrettyPrinter/IOutputAstVisitor.cs,
	  Project/Src/PrettyPrinter/AbstractOutputFormatter.cs,
	  Project/Src/PrettyPrinter/VBNet/VBNetOutputVisitor.cs: Added some
	  stuff for pre processor directive handling.

2008-09-12  Lluis Sanchez Gual <lluis@novell.com> 

	* NRefactory.mdp: Updated.

2008-09-10  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs: fixing comment bug.

2008-09-04  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs: fix for comment begin line.

2008-09-04  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs: Fixed comment start line.

2008-07-28  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs,
	  Project/Src/Lexer/Special/SpecialTracker.cs,
	  Project/Src/Lexer/Special/TagComment.cs,
	  Project/Src/Lexer/Special/Comment.cs,
	  Project/Src/Lexer/VBNet/Lexer.cs: Applied some custom nrefactory
	  changes.

2008-07-13  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Tokens.cs: Backported some nrefactory
	  changes.

2008-07-10  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Tokens.cs: Added some stuff from newer
	  nrefactory. To support c# 3.0 we need to update it to a newer
	  version (when the new dom works I'll do it - our changes in
	  nrefactory  aren't used anymore because all information is
	  retrieved by mcs and nrefactory only get's used as an expression
	  parser).

2008-04-22  Michael Hutchinson  <mhutchinson@novell.com>

	* Project/Src/Lexer/CSharp/Lexer.cs: Remove debug console write. 

2008-04-18  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs: fixed 'Bug 379219 - Code folding
	  issue with multiple comment lines'.

2008-04-10  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs,
	  Project/Src/Lexer/Special/SpecialTracker.cs,
	  Project/Src/Lexer/Special/TagComment.cs,
	  Project/Src/Lexer/Special/Comment.cs,
	  Project/Src/Lexer/VBNet/Lexer.cs: Comments now know if they start a
	  line.

2008-04-02  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs, Project/Src/Ast/Generated.cs:
	  fixed comment positioning.

2008-03-31  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs: Corrected comment end position.

2007-11-16  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Parser/CSharp/cs.ATG, Project/Src/Parser/CSharp/Parser.cs,
	  Project/Src/Parser/CSharp/CSharpParser.cs: Fixed "Bug 341330 - Problem
	  with code completion and Generics".

2007-10-23  Lluis Sanchez Gual <lluis@novell.com> 

	* NRefactory.mdp: Project file names updated by change in MD path functions.

2007-10-09  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs: fixed compilation error.

2007-10-09  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs: Fixed "Bug 331772 - Unable to handle
	  conditional class definition".

2007-10-09  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs: C# lexer can now handle #if ... #else
	  ... #endif pre processing directives.
	
	  (Needed to fix bug 331772 - Unable to handle conditional class
	  definition)

2007-09-15  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Parser/CSharp/cs.ATG, Project/Src/Parser/CSharp/Parser.cs:
	  Fixed 82841: Enum members appear twice in autocomplete.

2007-09-13  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Ast/General/CompilationUnit.cs: Compilation unit ToString
	  works.

2007-09-12  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Parser/CSharp/cs.ATG: Corrected some expression positions.

2007-09-11  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Parser/CSharp/cs.ATG: Corrected enum field end location. 

2007-09-07  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Parser/CSharp/cs.ATG, Project/Src/Parser/CSharp/Parser.cs:
	  Worked on bug 82720: Null ref when autocomplete activated

2007-09-06  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Parser/CSharp/cs.ATG, Project/Src/Parser/gen.sh: Applied some
	  patches to the new NRefartory from the former monodevelop nrefactory
	  version.
	
	  Added a parser generation script for linux which can be used to generate
	  the parser out of the ATG files. The Parser.cs files should never be
	  patched directly.

2007-09-04  Michael Hutchinson <MHutchinson@novell.com> 

	* Project/Src/Visitors/CodeDOMOutputVisitor.cs: Respect "partial" modifier
	  when building CodeDOM types.

2007-08-23  Mike Krüger <mkrueger@novell.com> 

	* NRefactory.mdp: Updated to the latest .NET 2.0 version.

2007-08-22  Mike Krüger <mkrueger@novell.com> 

	* Project/Src/Lexer/CSharp/Lexer.cs: Fixed 82529:TaskList doesn't update
	  when using comments: /**/.

2007-07-24  Jeffrey Stedfast  <fejj@novell.com>

	Part of the fix for bug #82114.

	* Project/Src/Parser/AST/AbstractNode.cs (AddChild): Set the
	Parent.

	* Project/Src/Parser/CSharp/cs.ATG: Same fixes as below... just in
	case we ever re-autogenerate Parser.cs

	* Project/Src/Parser/CSharp/Parser.cs: Store the location of
	VariableDeclarations and TypeReferences. Also make sure to set the
	TypeReference on each VariableDeclaration.

2007-02-15  Lluis Sanchez Gual <lluis@novell.com> 

	* Makefile.am, NRefactory.mdp: Disabled building through makefile. Set
	  target runtime to 2.0.

2007-02-08  Lluis Sanchez Gual <lluis@novell.com> 

	* Makefile.am, NRefactory.mdp: Added MD project and synchronized it
	  with the makefile.

2007-02-01  Lluis Sanchez Gual <lluis@novell.com> 

	* Project/Src/Parser/CSharp/Parser.cs: Store the location of field
	  reference expressions.

2006-12-07 Jacob Ilsø Christensen  <jacobilsoe@gmail.com>

	*  Project/NRefactory.csproj: Removed GlobalAssemblyInfo.cs
	since it is not used.

2006-07-03 Lluis Sanchez Gual  <lluis@novell.com> 

	* Project/Src/Parser/CSharp/Parser.cs: Store the location
	  of identifiers.

2006-05-22 Lluis Sanchez Gual  <lluis@novell.com> 

	* Makefile.am: Install the assembly in the right place.

2006-05-22 Lluis Sanchez Gual  <lluis@novell.com>

	Imported from SharpDevelop.
