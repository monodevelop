// Autogenerated from MonoDevelop.Core.Gui.addin.xml

using System.Reflection;

[assembly: AssemblyProduct ("MonoDevelop")]
[assembly: AssemblyTitle ("MonoDevelop Gui Services")]
[assembly: AssemblyDescription ("Provides basic GUI services")]
[assembly: AssemblyVersion ("1.9.2")]
[assembly: AssemblyCopyright ("GPL")]
