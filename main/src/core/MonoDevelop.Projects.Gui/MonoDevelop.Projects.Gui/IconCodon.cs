//  IconCodon.cs
//
//  This file was derived from a file from #Develop. 
//
//  Copyright (C) 2001-2007 Mike Krüger <mkrueger@novell.com>
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Collections;
using System.Reflection;
using System.ComponentModel;
using Mono.Addins;

using MonoDevelop.Core;

namespace MonoDevelop.Projects.Gui
{
	[ExtensionNode ("Icon", "An icon bound to a language or file extension")]
	internal class IconCodon : ExtensionNode
	{
		[NodeAttribute("language", "Name of the language represented by this icon. Optional.")]
		string language  = null;
		
		[NodeAttribute("icon", "Icon or resource name.")]
		string resource  = null;
		
		[NodeAttribute("extensions", "File extensions represented by this icon. Optional.")]
		string[] extensions = null;
		
		public string Language {
			get {
				return language;
			}
			set {
				language = value;
			}
		}
		
		public string Resource {
			get {
				return resource;
			}
			set {
				resource = value;
			}
		}
		
		public string[] Extensions {
			get {
				return extensions;
			}
			set {
				extensions = value;
			}
		}
	}
}
