// Autogenerated from MonoDevelop.Projects.Gui.addin.xml

using System.Reflection;

[assembly: AssemblyProduct ("MonoDevelop")]
[assembly: AssemblyTitle ("MonoDevelop GUI for Project Services")]
[assembly: AssemblyDescription ("Provides GUI resources for managing MonoDevelop projects")]
[assembly: AssemblyVersion ("1.9.2")]
[assembly: AssemblyCopyright ("GPL")]
