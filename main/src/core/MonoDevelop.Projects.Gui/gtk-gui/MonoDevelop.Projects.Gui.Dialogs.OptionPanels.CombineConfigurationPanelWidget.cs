// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace MonoDevelop.Projects.Gui.Dialogs.OptionPanels {
    
    
    internal partial class CombineConfigurationPanelWidget {
        
        private Gtk.VBox vbox74;
        
        private Gtk.HBox hbox60;
        
        private Gtk.Label label104;
        
        private Gtk.ScrolledWindow scrolledwindow11;
        
        private Gtk.TreeView configsList;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget MonoDevelop.Projects.Gui.Dialogs.OptionPanels.CombineConfigurationPanelWidget
            Stetic.BinContainer.Attach(this);
            this.Name = "MonoDevelop.Projects.Gui.Dialogs.OptionPanels.CombineConfigurationPanelWidget";
            // Container child MonoDevelop.Projects.Gui.Dialogs.OptionPanels.CombineConfigurationPanelWidget.Gtk.Container+ContainerChild
            this.vbox74 = new Gtk.VBox();
            this.vbox74.Name = "vbox74";
            this.vbox74.Spacing = 6;
            // Container child vbox74.Gtk.Box+BoxChild
            this.hbox60 = new Gtk.HBox();
            this.hbox60.Name = "hbox60";
            this.hbox60.Spacing = 6;
            // Container child hbox60.Gtk.Box+BoxChild
            this.label104 = new Gtk.Label();
            this.label104.Name = "label104";
            this.label104.LabelProp = MonoDevelop.Core.GettextCatalog.GetString("Select a target configuration for each solution item:");
            this.hbox60.Add(this.label104);
            Gtk.Box.BoxChild w1 = ((Gtk.Box.BoxChild)(this.hbox60[this.label104]));
            w1.Position = 0;
            w1.Expand = false;
            w1.Fill = false;
            this.vbox74.Add(this.hbox60);
            Gtk.Box.BoxChild w2 = ((Gtk.Box.BoxChild)(this.vbox74[this.hbox60]));
            w2.Position = 0;
            w2.Expand = false;
            // Container child vbox74.Gtk.Box+BoxChild
            this.scrolledwindow11 = new Gtk.ScrolledWindow();
            this.scrolledwindow11.Name = "scrolledwindow11";
            this.scrolledwindow11.ShadowType = ((Gtk.ShadowType)(1));
            // Container child scrolledwindow11.Gtk.Container+ContainerChild
            this.configsList = new Gtk.TreeView();
            this.configsList.Name = "configsList";
            this.scrolledwindow11.Add(this.configsList);
            this.vbox74.Add(this.scrolledwindow11);
            Gtk.Box.BoxChild w4 = ((Gtk.Box.BoxChild)(this.vbox74[this.scrolledwindow11]));
            w4.Position = 1;
            this.Add(this.vbox74);
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.Show();
        }
    }
}
