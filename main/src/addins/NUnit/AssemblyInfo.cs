// Autogenerated from MonoDevelopNUnit.addin.xml

using System.Reflection;

[assembly: AssemblyProduct ("MonoDevelop")]
[assembly: AssemblyTitle ("NUnit support")]
[assembly: AssemblyDescription ("Integrates NUnit into the MonoDevelop IDE.")]
[assembly: AssemblyVersion ("1.9.2")]
[assembly: AssemblyCopyright ("GPL")]
