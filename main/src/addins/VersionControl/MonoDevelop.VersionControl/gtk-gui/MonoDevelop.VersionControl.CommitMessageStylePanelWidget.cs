// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace MonoDevelop.VersionControl {
    
    
    public partial class CommitMessageStylePanelWidget {
        
        private Gtk.VBox vbox1;
        
        private Gtk.Table table2;
        
        private Gtk.Entry entryHeader;
        
        private Gtk.Label label4;
        
        private Gtk.Table tableFlags;
        
        private Gtk.CheckButton checkIndent;
        
        private Gtk.CheckButton checkIndentEntries;
        
        private Gtk.CheckButton checkLineSep;
        
        private Gtk.CheckButton checkMsgInNewLine;
        
        private Gtk.CheckButton checkOneLinePerFile;
        
        private Gtk.CheckButton checkUseBullets;
        
        private Gtk.Label label9;
        
        private Gtk.ScrolledWindow GtkScrolledWindow;
        
        private Gtk.TextView textview;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget MonoDevelop.VersionControl.CommitMessageStylePanelWidget
            Stetic.BinContainer.Attach(this);
            this.Name = "MonoDevelop.VersionControl.CommitMessageStylePanelWidget";
            // Container child MonoDevelop.VersionControl.CommitMessageStylePanelWidget.Gtk.Container+ContainerChild
            this.vbox1 = new Gtk.VBox();
            this.vbox1.Name = "vbox1";
            this.vbox1.Spacing = 6;
            // Container child vbox1.Gtk.Box+BoxChild
            this.table2 = new Gtk.Table(((uint)(3)), ((uint)(2)), false);
            this.table2.Name = "table2";
            this.table2.RowSpacing = ((uint)(6));
            this.table2.ColumnSpacing = ((uint)(6));
            // Container child table2.Gtk.Table+TableChild
            this.entryHeader = new Gtk.Entry();
            this.entryHeader.CanFocus = true;
            this.entryHeader.Name = "entryHeader";
            this.entryHeader.IsEditable = true;
            this.entryHeader.InvisibleChar = '●';
            this.table2.Add(this.entryHeader);
            Gtk.Table.TableChild w1 = ((Gtk.Table.TableChild)(this.table2[this.entryHeader]));
            w1.LeftAttach = ((uint)(1));
            w1.RightAttach = ((uint)(2));
            w1.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table2.Gtk.Table+TableChild
            this.label4 = new Gtk.Label();
            this.label4.Name = "label4";
            this.label4.Xalign = 0F;
            this.label4.LabelProp = Mono.Unix.Catalog.GetString("Message Header:");
            this.table2.Add(this.label4);
            Gtk.Table.TableChild w2 = ((Gtk.Table.TableChild)(this.table2[this.label4]));
            w2.XOptions = ((Gtk.AttachOptions)(4));
            w2.YOptions = ((Gtk.AttachOptions)(4));
            this.vbox1.Add(this.table2);
            Gtk.Box.BoxChild w3 = ((Gtk.Box.BoxChild)(this.vbox1[this.table2]));
            w3.Position = 0;
            w3.Expand = false;
            w3.Fill = false;
            // Container child vbox1.Gtk.Box+BoxChild
            this.tableFlags = new Gtk.Table(((uint)(3)), ((uint)(2)), false);
            this.tableFlags.Name = "tableFlags";
            this.tableFlags.RowSpacing = ((uint)(6));
            this.tableFlags.ColumnSpacing = ((uint)(6));
            // Container child tableFlags.Gtk.Table+TableChild
            this.checkIndent = new Gtk.CheckButton();
            this.checkIndent.CanFocus = true;
            this.checkIndent.Name = "checkIndent";
            this.checkIndent.Label = Mono.Unix.Catalog.GetString("Align message text");
            this.checkIndent.DrawIndicator = true;
            this.checkIndent.UseUnderline = true;
            this.tableFlags.Add(this.checkIndent);
            Gtk.Table.TableChild w4 = ((Gtk.Table.TableChild)(this.tableFlags[this.checkIndent]));
            w4.TopAttach = ((uint)(1));
            w4.BottomAttach = ((uint)(2));
            w4.YOptions = ((Gtk.AttachOptions)(4));
            // Container child tableFlags.Gtk.Table+TableChild
            this.checkIndentEntries = new Gtk.CheckButton();
            this.checkIndentEntries.CanFocus = true;
            this.checkIndentEntries.Name = "checkIndentEntries";
            this.checkIndentEntries.Label = Mono.Unix.Catalog.GetString("Indent entries");
            this.checkIndentEntries.DrawIndicator = true;
            this.checkIndentEntries.UseUnderline = true;
            this.tableFlags.Add(this.checkIndentEntries);
            Gtk.Table.TableChild w5 = ((Gtk.Table.TableChild)(this.tableFlags[this.checkIndentEntries]));
            w5.LeftAttach = ((uint)(1));
            w5.RightAttach = ((uint)(2));
            w5.YOptions = ((Gtk.AttachOptions)(4));
            // Container child tableFlags.Gtk.Table+TableChild
            this.checkLineSep = new Gtk.CheckButton();
            this.checkLineSep.CanFocus = true;
            this.checkLineSep.Name = "checkLineSep";
            this.checkLineSep.Label = Mono.Unix.Catalog.GetString("Add a blank line between messages");
            this.checkLineSep.DrawIndicator = true;
            this.checkLineSep.UseUnderline = true;
            this.tableFlags.Add(this.checkLineSep);
            Gtk.Table.TableChild w6 = ((Gtk.Table.TableChild)(this.tableFlags[this.checkLineSep]));
            w6.TopAttach = ((uint)(1));
            w6.BottomAttach = ((uint)(2));
            w6.LeftAttach = ((uint)(1));
            w6.RightAttach = ((uint)(2));
            w6.YOptions = ((Gtk.AttachOptions)(4));
            // Container child tableFlags.Gtk.Table+TableChild
            this.checkMsgInNewLine = new Gtk.CheckButton();
            this.checkMsgInNewLine.CanFocus = true;
            this.checkMsgInNewLine.Name = "checkMsgInNewLine";
            this.checkMsgInNewLine.Label = Mono.Unix.Catalog.GetString("File list and message in separate lines");
            this.checkMsgInNewLine.DrawIndicator = true;
            this.checkMsgInNewLine.UseUnderline = true;
            this.tableFlags.Add(this.checkMsgInNewLine);
            Gtk.Table.TableChild w7 = ((Gtk.Table.TableChild)(this.tableFlags[this.checkMsgInNewLine]));
            w7.TopAttach = ((uint)(2));
            w7.BottomAttach = ((uint)(3));
            w7.LeftAttach = ((uint)(1));
            w7.RightAttach = ((uint)(2));
            w7.YOptions = ((Gtk.AttachOptions)(4));
            // Container child tableFlags.Gtk.Table+TableChild
            this.checkOneLinePerFile = new Gtk.CheckButton();
            this.checkOneLinePerFile.CanFocus = true;
            this.checkOneLinePerFile.Name = "checkOneLinePerFile";
            this.checkOneLinePerFile.Label = Mono.Unix.Catalog.GetString("One line per file");
            this.checkOneLinePerFile.DrawIndicator = true;
            this.checkOneLinePerFile.UseUnderline = true;
            this.tableFlags.Add(this.checkOneLinePerFile);
            Gtk.Table.TableChild w8 = ((Gtk.Table.TableChild)(this.tableFlags[this.checkOneLinePerFile]));
            w8.TopAttach = ((uint)(2));
            w8.BottomAttach = ((uint)(3));
            w8.YOptions = ((Gtk.AttachOptions)(4));
            // Container child tableFlags.Gtk.Table+TableChild
            this.checkUseBullets = new Gtk.CheckButton();
            this.checkUseBullets.CanFocus = true;
            this.checkUseBullets.Name = "checkUseBullets";
            this.checkUseBullets.Label = Mono.Unix.Catalog.GetString("Use bullets");
            this.checkUseBullets.DrawIndicator = true;
            this.checkUseBullets.UseUnderline = true;
            this.tableFlags.Add(this.checkUseBullets);
            Gtk.Table.TableChild w9 = ((Gtk.Table.TableChild)(this.tableFlags[this.checkUseBullets]));
            w9.YOptions = ((Gtk.AttachOptions)(4));
            this.vbox1.Add(this.tableFlags);
            Gtk.Box.BoxChild w10 = ((Gtk.Box.BoxChild)(this.vbox1[this.tableFlags]));
            w10.Position = 1;
            w10.Expand = false;
            w10.Fill = false;
            // Container child vbox1.Gtk.Box+BoxChild
            this.label9 = new Gtk.Label();
            this.label9.Name = "label9";
            this.label9.Xalign = 0F;
            this.label9.LabelProp = Mono.Unix.Catalog.GetString("Preview:");
            this.vbox1.Add(this.label9);
            Gtk.Box.BoxChild w11 = ((Gtk.Box.BoxChild)(this.vbox1[this.label9]));
            w11.Position = 2;
            w11.Expand = false;
            w11.Fill = false;
            // Container child vbox1.Gtk.Box+BoxChild
            this.GtkScrolledWindow = new Gtk.ScrolledWindow();
            this.GtkScrolledWindow.Sensitive = false;
            this.GtkScrolledWindow.Name = "GtkScrolledWindow";
            this.GtkScrolledWindow.VscrollbarPolicy = ((Gtk.PolicyType)(2));
            this.GtkScrolledWindow.HscrollbarPolicy = ((Gtk.PolicyType)(2));
            this.GtkScrolledWindow.ShadowType = ((Gtk.ShadowType)(1));
            // Container child GtkScrolledWindow.Gtk.Container+ContainerChild
            this.textview = new Gtk.TextView();
            this.textview.CanFocus = true;
            this.textview.Name = "textview";
            this.GtkScrolledWindow.Add(this.textview);
            this.vbox1.Add(this.GtkScrolledWindow);
            Gtk.Box.BoxChild w13 = ((Gtk.Box.BoxChild)(this.vbox1[this.GtkScrolledWindow]));
            w13.Position = 3;
            this.Add(this.vbox1);
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.Hide();
            this.entryHeader.Changed += new System.EventHandler(this.OnEntryHeaderChanged);
            this.checkUseBullets.Toggled += new System.EventHandler(this.OnCheckUseBulletsToggled);
            this.checkOneLinePerFile.Toggled += new System.EventHandler(this.OnCheckOneLinePerFileToggled);
            this.checkMsgInNewLine.Toggled += new System.EventHandler(this.OnCheckMsgInNewLineToggled);
            this.checkLineSep.Toggled += new System.EventHandler(this.OnCheckLineSepToggled);
            this.checkIndentEntries.Toggled += new System.EventHandler(this.OnCheckIndentEntriesToggled);
            this.checkIndent.Toggled += new System.EventHandler(this.OnCheckIndentToggled);
        }
    }
}
