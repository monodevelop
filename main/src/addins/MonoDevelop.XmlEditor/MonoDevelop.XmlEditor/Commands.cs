//
// MonoDevelop XML Editor
//
// Copyright (C) 2006 Matthew Ward
//

using MonoDevelop.Components.Commands;
using MonoDevelop.Core.Gui;
using MonoDevelop.Ide.Gui;
using System;

namespace MonoDevelop.XmlEditor
{
	public enum Commands
	{	
		CreateSchema,
		Validate,
		AssignStylesheet,
		OpenStylesheet,
		RunXslTransform,
		Format,
		OpenXPathQueryPad,
		RemoveXPathHighlighting,
		GoToSchemaDefinition,
		ContextualMainMenu
	}
}
