2009-02-27  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionCSharp3Tests.cs:
	  Added unit test for "Bug 479983 - No C# completion for
	  object initializers".

2009-02-26  Lluis Sanchez Gual  <lluis@novell.com>

	* UnitTests.csproj: Flush.

2009-02-24  Mike Krüger  <mkrueger@novell.com>

	* Mono.TextEditor.Tests/SyntaxHighlightingTests.cs: Added some
	  highlighting tests.

2009-02-23  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs:
	  Added more unit tests do document resolver issues.

2009-02-23  Lluis Sanchez Gual  <lluis@novell.com>

	* TestBase.cs: Use a local directory for the add-in registry.

2009-02-23  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs:
	  Added tests for implicit generic method parameter.

2009-02-23  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs:
	  Added test for explicit generic method parameters.

2009-02-23  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionCSharp3Tests.cs:
	  fixed typo in unit test.

2009-02-23  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs:
	  Added test for inner type bug.

2009-02-23  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs:
	  Added test for inner inner type access.

2009-02-23  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs:
	  Added test for AttributeArguments expression context.

2009-02-13  Michael Hutchinson  <mhutchinson@novell.com>

	* Makefile.am: MD broke the makefile; fix it.

2009-02-13  Michael Hutchinson  <mhutchinson@novell.com>

	* Makefile.am:
	* UnitTests.csproj:
	* MonoDevelop.Xml.StateEngine/TestParser.cs:
	* MonoDevelop.Xml.StateEngine/ParsingTests.cs:
	* MonoDevelop.Xml.StateEngine/HtmlParsingTests.cs:
	* MonoDevelop.Xml.StateEngine/AspNetParsingTests.cs: Add more
	  tests.

2009-02-12  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.CSharpBinding/TestWorkbenchWindow.cs: Track api
	  changes.

2009-02-12  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs:
	  Added test case to document a regression bug.

2009-02-11  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.Projects/TestProjectsChecks.cs: Order on which
	  items are added is now important.

	* TestBase.cs: Use a private home dir for the test suite to
	  make sure that local configurations don't affect the
	  results.

	* UnitTests.csproj: Updated.

2009-02-11  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Another
	  test for bug #470954

2009-02-11  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs:
	  Added more namespace access tests.

2009-02-11  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: added
	  test for "Bug 470954 - using System.Windows.Forms is not
	  honored".

2009-02-10  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added
	  test for "Bug 350862 - Autocomplete bug with enums".

2009-02-10  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs:
	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: added
	  tests for "Bug 474199 - Code completion not working for a
	  nested class". Refactored parameter completion tests -
	  they're now using the $...$ syntax too.

2009-02-10  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs:
	  Added namespace access tests.

2009-02-10  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionCSharpTests.cs:
	  Added test case for new "as" completion context.

2009-02-09  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: added
	  test for "Bug 473849 - Classes with no visible constructor
	  shouldn't appear in "new" completion".

2009-02-09  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.Projects/CompletionDatabaseTests.cs: Properly
	  fix warning.

2009-02-09  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added
	  test for "Bug 473686 - Constants are not included in code
	  completion".

2009-02-09  Mike Krüger  <mkrueger@novell.com>

	* Mono.TextEditor.Tests/SelectionTests.cs:
	* MonoDevelop.Xml.StateEngine/TestParser.cs:
	* MonoDevelop.CSharpBinding/TestViewContent.cs:
	* MonoDevelop.Projects/CompletionDatabaseTests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs:
	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: Fixed
	  compiler warnings.

2009-02-08  Michael Hutchinson  <mhutchinson@novell.com>

	* UnitTests.csproj: Remove unwanted copy-to-output.

2009-02-08  Michael Hutchinson  <mhutchinson@novell.com>

	* UnitTests.csproj: Fix referenced GTK# version.

2009-02-08  Michael Hutchinson  <mhutchinson@novell.com>

	* Makefile.am:
	* UnitTests.csproj: More refs to keep csc happy.

2009-02-06  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionCSharp3Tests.cs:
	  Adding C#3 query expression test.

2009-02-06  Mike Krüger  <mkrueger@novell.com>

	* Makefile.am:
	* UnitTests.csproj: 

	* Mono.TextEditor.Tests/SyntaxHighlightingTests.cs: added
	  syntax highlighting tests.

2009-02-06  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: fixed
	  unit test.

2009-02-06  Lluis Sanchez Gual  <lluis@novell.com>

	* UnitTests.mdp:
	* UnitTests.csproj: Migrated to MSBuild file format.

2009-02-05  Lluis Sanchez Gual  <lluis@novell.com>

	* Makefile.am: Removed incorrect nunit reference.

2009-02-05  Mike Krüger  <mkrueger@novell.com>

	* Mono.TextEditor.Tests/DocumentTests.cs:
	* MonoDevelop.CSharpBinding/TestViewContent.cs: Changed api.

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added
	  test for "Bug 471937 - Code completion of 'new' showing
	  invorrect entries".

2009-02-04  Lluis Sanchez Gual  <lluis@novell.com>

	* Makefile.am:
	* UnitTests.mdp:
	* MonoDevelop.Projects/TextFormatterTests.cs: Added tests for
	  TextFormatter.

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: New
	  test.

2009-02-04  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: added
	  test for 'Bug 471935 - Code completion window not showing
	  in MD1CustomDataItem.cs'.

2009-01-28  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: fixed bug in the
	test suite.

2009-01-27  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.Projects/CompletionDatabaseTests.cs: Added test for
	partial classes.

	* UnitTests.mdp: Updated.

2009-01-27  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.Projects/CompletionDatabaseTests.cs: Updated tests.

2009-01-26  Michael Hutchinson  <mhutchinson@novell.com>

	* UnitTests.mdp: Flush project format changes.

2009-01-27  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.Projects/CompletionDatabaseTests.cs: Added unit tests for
	checking generic constraints.

	* Makefile.am:
	* UnitTests.mdp: Updated.

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionCSharpTests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionCSharp3Tests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionOperatorTests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs: Use the
	real database backed ProjectDom, instead of SimpleProjectDom. Tests
	are more reliable in this way. However, this requires some changes.
	The ProjectDom database is only updated when a file is parsed without
	errors. Many of the code snippets used in the unit tests are not
	fully parseable because they contain code partially completed (e.g.
	like "blah. "). The solution is to enclose all that partial code
	inside $ signs. So now $ has a different meaning. Instead of the
	cursor position, it means 'code written before triggering code
	completion'.

2009-01-20  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.Projects/CompletionDatabaseTests.cs: Added new test.

2009-01-20  Lluis Sanchez Gual  <lluis@novell.com>

	* Mono.TextEditor.Tests/SearchTests.cs: Track api changes.

2009-01-20  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: added unit test
	for "Bug 467507 - No completion of base members inside explicit
	events".

2009-01-20  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs: Added some
	tests to document bug "Bug 467409 - Bad code completion options".

2009-01-20  Mike Krüger  <mkrueger@novell.com>

	* Mono.TextEditor.Tests/FoldingTests.cs: Added folding unit tests.

2009-01-16  Lluis Sanchez Gual  <lluis@novell.com>

	* Makefile.am:
	* UnitTests.mdp: Update references.

2009-01-16  Lluis Sanchez Gual  <lluis@novell.com>

	* Makefile.am: Use correct noshadow option.

	* TestBase.cs: Delete the temp dir when a test run starts.

2009-01-16  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/FindMemberVisitorTests.cs: Added some tests.

2009-01-16  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/FindMemberVisitorTests.cs: Activated find
	event reference test.

2009-01-16  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs: Added
	generic parameter unit tests.

2009-01-16  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added test for
	"Bug 466692 - Missing completion for return/break keywords after
	yield".

2009-01-15  Michael Hutchinson  <mhutchinson@novell.com>

	* MonoDevelop.Projects/LocalCopyTests.cs: Test local-copy of external
	files. Patch from Sam Chuparkoff.

2009-01-15  Mike Krüger  <mkrueger@novell.com>

	* UnitTests.mdp: unit tests are now working again.

2009-01-13  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/FindMemberVisitorTests.cs: added find event
	tests. 

2009-01-12  Michael Hutchinson  <mhutchinson@novell.com>

	* MonoDevelop.Projects/LocalCopyTests.cs: Update local-copy test to make
	sure paths are included. Covers "Bug 459311 - Copying files to output
	directory in MonoDevelop".

2009-01-09  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs:
	* MonoDevelop.CSharpBinding/FindMemberVisitorTests.cs: Added unit tests
	for the refactoring layer.

2009-01-07  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added test for
	Bug 459682 - Static methods/properties don't show up in subclasses.

2009-01-06  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added unit test
	for 'Bug 457237 - code completion doesn't show static methods when
	setting global variable'.

2009-01-06  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added test for
	'Bug 457003 - code completion shows variables out of scope'.

2009-01-05  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added test for
	'Bug 460234 - Invalid options shown when typing 'override''.

2008-12-16  Lluis Sanchez Gual  <lluis@novell.com>

	* Makefile.am:
	* UnitTests.mdp:
	* MonoDevelop.Projects/CompletionDatabaseTests.cs: Added new tests for
	the code completion database.

2008-12-12  Lluis Sanchez Gual  <lluis@novell.com>

	* Makefile.am:
	* UnitTests.mdp: Fix target framework and some references.

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs: Set
	correct base class.

2008-11-24  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: added unit test
	for 'Bug 447985 - Exception display tip is inaccurate for derived
	(custom) exceptions'.

2008-11-18  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Fixed unit tests.

2008-11-14  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: added test for
	bug 4441110

2008-11-07  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.Projects/ProjectTests.cs: Fix incorrect satellit assembly
	name.

2008-11-05  Mike Krüger  <mike@icsharpcode.net>

	* Mono.TextEditor.Tests/LineSplitterTests.cs: some cosmetic changes.

2008-11-05  Mike Krüger  <mike@icsharpcode.net>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added test for
	'Bug 441671 - Finalisers show up in code completion'.

2008-11-03  Mike Krüger  <mike@icsharpcode.net>

	* MonoDevelop.CSharpBinding/CodeCompletionCSharpTests.cs: added some
	basic unit tests.

2008-10-31  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.Projects/ProjectTests.cs: Add unit test for bug #400420.

2008-10-31  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: added unit test
	for 'Bug 439963 - Lacking members in code completion'.

2008-10-30  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.Projects/ProjectTests.cs: Added some bug tests.

2008-10-30  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: added more cases
	from 'Bug 432434 - Code completion doesn't work with subclasses'.

2008-10-30  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: Added test for
	'Bug 436705 - code completion for constructors does not handle class
	name collisions properly'.

2008-10-30  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs: added test for
	'Bug 432434 - Code completion doesn't work with subclasses'.

2008-10-29  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionBugTests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionCSharpTests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionCSharp3Tests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionOperatorTests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs: Added some
	unit tests.

2008-10-29  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added  unit test for
	"Bug 439601 - Intellisense Broken For Partial Classes".

2008-10-27  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: fixed a bug in
	unit tests.

2008-10-27  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: Added test for
	"Bug 434705 - No autocomplete offered if not assigning result of
	'new' to a variable".

2008-10-23  Michael Hutchinson  <mhutchinson@novell.com>

	* MonoDevelop.Xml.StateEngine/TestParser.cs: Track error API.

2008-10-23  Lluis Sanchez Gual  <lluis@novell.com>

	* MonoDevelop.CSharpBinding/TopLevelTests.cs: Added attribute parsing
	tests.

	* MonoDevelop.Projects/MSBuildTests.cs: Added test for merging of
	configuration properties.

2008-10-19  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: made attribute
	unit test a bit harder.

2008-10-16  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionAccessibleTests.cs: Added some
	unit tests.

2008-10-16 Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: Added unit test
	for 'Bug 434701 - No autocomplete in attributes'.

2008-10-16 Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: added unit test
	for 'Bug 434705 - No autocomplete offered if not assigning result of
	'new' to a variable'.

2008-10-15  Michael Hutchinson  <mhutchinson@novell.com>

	* MonoDevelop.Xml.StateEngine/ParsingTests.cs: Don't start up the MD
	runtime for parser tests.

2008-10-15  Michael Hutchinson  <mhutchinson@novell.com>

	* MonoDevelop.Xml.StateEngine/ParsingTests.cs: Improve tests using new
	TestParser utility class.

	* Makefile.am:
	* UnitTests.mdp: Updated.

	* MonoDevelop.Xml.StateEngine/TestParser.cs: Utility for testing the XML
	parser.

2008-10-15  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added test for 'Bug
	434770 - No autocomplete on array types'.

2008-10-14  Michael Hutchinson  <mhutchinson@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionCSharp3Tests.cs:
	* MonoDevelop.CSharpBinding/CodeCompletionOperatorTests.cs: Track API.

2008-10-10  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: Added unit test for
	bug 431803.

2008-10-09  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added test.

2008-10-08  Michael Hutchinson  <mhutchinson@novell.com>

	* Makefile.am:
	* UnitTests.mdp:
	* MonoDevelop.Xml.StateEngine:
	* MonoDevelop.Xml.StateEngine/ParsingTests.cs: Add some simple tests for
	the XML state engine.

2008-10-08  Mike Krüger  <mkrueger@novell.com>

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added test for
	432681

2008-10-07  Michael Hutchinson <mhutchinson@novell.com> 

	* Mono.TextEditor.Tests.DefaultEditActions/CaretMoveTests.cs,
	  Mono.TextEditor.Tests.DefaultEditActions/InsertNewLineTests.cs,
	  Mono.TextEditor.Tests.DefaultEditActions/InsertTabTests.cs,
	  Mono.TextEditor.Tests.DefaultEditActions/RemoveTabTests.cs,
	  Mono.TextEditor.Tests.DefaultEditActions/SelectionSelectAllTests.cs:
	  Track API.

2008-10-07  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: added unit
	  test for Bug 432727 - No completion if no constructor.

2008-10-06  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: fixed 432658.

2008-10-06  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added unit tests
	  for 'Bug 431797 - Code completion showing invalid options'.

2008-10-06  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added unit test for
	  Bug 431764 - Completion doesn't work in properties.

2008-10-06  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: added unit
	  test for 'Bug 432437 - No completion when invoking delegates'.

2008-10-01  Michael Hutchinson <mhutchinson@novell.com> 

	* MonoDevelop.Projects/DomCompilationUnitTests.cs,
	  MonoDevelop.Projects/DomPersistenceTests.cs,
	  MonoDevelop.Projects/ProjectTests.cs,
	  MonoDevelop.Projects/TestProjectsChecks.cs, UnitTests.mdp,
	  MonoDevelop.CSharpBinding/CodeCompletionCSharp3Tests.cs,
	  MonoDevelop.CSharpBinding/TopLevelTests.cs,
	  MonoDevelop.CSharpBinding/ParameterCompletionTests.cs,
	  MonoDevelop.CSharpBinding/CodeCompletionOperatorTests.cs,
	  MonoDevelop.CSharpBinding/CodeCompletionTests.cs,
	  MonoDevelop.CSharpBinding/MemberTests.cs, Makefile.am: Tweak to fix
	  tests build/run.
	* MonoDevelop.Projects/LocalCopyTests.cs: Add tests for local copy of
	  files and references.

2008-09-25  Mike Krüger <mkrueger@novell.com> 

	* Mono.TextEditor.Tests/DocumentTests.cs,
	  Mono.TextEditor.Tests/LineSplitterTests.cs,
	  Mono.TextEditor.Tests/UndoRedoTests.cs,
	  Mono.TextEditor.Tests/SearchTests.cs,
	  Mono.TextEditor.Tests/SelectionTests.cs,
	  Mono.TextEditor.Tests.DefaultEditActions/InsertTabTests.cs,
	  Mono.TextEditor.Tests.DefaultEditActions/SelectionSelectAllTests.cs,
	  Mono.TextEditor.Tests.DefaultEditActions/RemoveTabTests.cs,
	  Mono.TextEditor.Tests.DefaultEditActions/CaretMoveTests.cs,
	  Mono.TextEditor.Tests.DefaultEditActions/InsertNewLineTests.cs:
	  Added/updated text editor unit tests.

2008-09-24  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionCSharp3Tests.cs: added some
	  c# 3.0 unit tests.

2008-09-24  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionCSharp3Tests.cs: Added C#3.0
	  unit test fixture.

2008-09-23  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: extended
	  constructor parameter test.

2008-09-23  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/ParameterCompletionTests.cs: Added test for
	  'Bug 427448 - Code Completion: completion of constructor parameters
	  not working'.

2008-09-22  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added test case for
	  interface code completion bug.

2008-09-20  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionOperatorTests.cs,
	  MonoDevelop.CSharpBinding/CodeCompletionTests.cs: Added operator
	  completion tests.

2008-09-19  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added unit tests
	  for 'Bug 427734 - Code Completion issues with enums'.

2008-09-19  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added unit test for
	  'Bug 427649 - Code Completion: protected methods shown in code
	  completion'.

2008-09-19  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added unit test for
	  'Bug 405000 - Namespace alias qualifier operator (::) does not
	  trigger code completion'.

2008-09-19  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.Projects/DomPersistenceTests.cs: Extended persistance
	  unit test to document a bug.

2008-09-18  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added unit test for
	  "Bug 427294 - Code Completion: completion on values returned by
	  methods doesn't work".

2008-09-18  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: added test for "Bug
	  427284 - Code Completion: class list shows the full name of
	  classes".

2008-09-17  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/CodeCompletionTests.cs: Added some unit
	  tests.

2008-09-17  Mike Krüger <mkrueger@novell.com> 

	* UnitTests.mdp, MonoDevelop.CSharpBinding/TestViewContent.cs,
	  MonoDevelop.CSharpBinding/CodeCompletionTests.cs,
	  MonoDevelop.CSharpBinding/TestWorkbenchWindow.cs, Makefile.am:
	  Added code completion unit tests.

2008-09-16  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/MemberTests.cs: Extended test constructor
	  test.

2008-09-10  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/MemberTests.cs: Added member unit tests.

2008-09-10  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/TopLevelTests.cs: changed compilation unit
	  interface.

2008-09-09  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.Projects/DomPersistenceTests.cs: Added unit a delegate
	  unit test.

2008-09-08  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.CSharpBinding/TopLevelTests.cs: Added interface test.

2008-09-08  Mike Krüger <mkrueger@novell.com> 

	* UnitTests.mdp, MonoDevelop.CSharpBinding,
	  MonoDevelop.CSharpBinding/TopLevelTests.cs, Makefile.am: Added dom
	  parser top level tests.

2008-09-05  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.Projects/DomPersistenceTests.cs: Added persistance tests.

2008-07-27  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.Projects/DomPersistenceTests.cs: Removed persistence
	  tests.

2008-07-21  Lluis Sanchez Gual <lluis@novell.com> 

	* MonoDevelop.Projects/TestProjectsChecks.cs: Moved serialization
	  engine to MonoDevelop.Core. Use new syntax for specifying attribute
	  scope.

2008-07-07  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.Projects/DomCompilationUnitTests.cs,
	  MonoDevelop.Projects/DomPersistenceTests.cs: Added some tests.

2008-06-18  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.Projects/DomPersistenceTests.cs: Worked on unit tests.

2008-06-18  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.Projects/DomPersistenceTests.cs: Added some dom
	  persistence tests.

2008-06-17  Mike Krüger <mkrueger@novell.com> 

	* MonoDevelop.Projects/DomPersistenceTests.cs: Added dom persistence
	  unit tests.

2008-06-16  Lluis Sanchez Gual <lluis@novell.com> 

	* MonoDevelop.Projects/SolutionTests.cs,
	  MonoDevelop.Projects/MSBuildTests.cs,
	  MonoDevelop.Projects/MdsTests.cs,
	  MonoDevelop.Projects/TestProjectsChecks.cs: Track api changes.

2008-06-04  Lluis Sanchez Gual <lluis@novell.com> 

	* MonoDevelop.Projects/SolutionTests.cs: New tests for checking
	  solution configuration mappings.

2008-06-04  Lluis Sanchez Gual <lluis@novell.com> 

	* MonoDevelop.Projects/MSBuildTests.cs: Test fixes.

2008-05-23  Lluis Sanchez Gual <lluis@novell.com> 

	* MonoDevelop.Projects/SolutionTests.cs, Util.cs: Added new test.

2008-05-23  Lluis Sanchez Gual <lluis@novell.com> 

	* MonoDevelop.Projects/TestProjectsChecks.cs, UnitTests.mdp: Use full
	  names when creating package references.

2008-05-22  Lluis Sanchez Gual <lluis@novell.com> 

	* MonoDevelop.Projects/SolutionTests.cs,
	  MonoDevelop.Projects/WorkspaceTests.cs,
	  MonoDevelop.Projects/MSBuildTests.cs,
	  MonoDevelop.Projects/MdsTests.cs,
	  MonoDevelop.Projects/TestProjectsChecks.cs: Replaced
	  ICompilerResult/DefaultCompilerResult/CompilerResults by a new
	  BuildResult class, which has owner information at error level, so
	  it is possible to know which project generated an error when
	  building a solution. Updated Task and TaskService to use the new
	  owner information.

2008-05-21  Lluis Sanchez Gual <lluis@novell.com> 

	* MonoDevelop.Projects/SolutionTests.cs,
	  MonoDevelop.Projects/MakefileTests.cs,
	  MonoDevelop.Projects/WorkspaceTests.cs,
	  MonoDevelop.Projects/MSBuildTests.cs,
	  MonoDevelop.Projects/MdsTests.cs,
	  MonoDevelop.Projects/PackagingTests.cs,
	  MonoDevelop.Projects/ProjectTests.cs,
	  MonoDevelop.Projects/MonoDeveloperFormatTests.cs,
	  MonoDevelop.Projects/TestProjectsChecks.cs, Util.cs, TestBase.cs:
	  New unit test infrastructure.


